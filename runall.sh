#!/bin/zsh

set -eu # makes your program exit on error or unbound variable

basedir=`pwd`

# install odexp
cd codes/odexp-v1.0.3/
rm -fr build
mkdir -p build && cd build
builddir=`pwd`
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$builddir ..
make 
make install
export PATH=$PATH:`pwd`/bin
library_path=`pwd`/lib
PKG_CONFIG_PATH+=`pwd`/share/pkgconfig
export PKG_CONFIG_PATH
export CFLAGS=`pkg-config --cflags-only-I odexp`
LFLAGS=`pkg-config --libs-only-L odexp` 
LFLAGS+=" -Wl,-rpath $library_path"
export LFLAGS
cd $basedir

# run simulations
./codes/batch_cohort_1.sh && ./codes/trim_virtual_cohort_1.sh
./codes/batch_cohort_2.sh && ./codes/trim_virtual_cohort_2.sh
./codes/trial_cohort_1.sh && ./codes/trim_cohort_1_trial.sh

# generate figures
cd figures
R -f generate_figures.R

cd $basedir 

exit 0
