#!/bin/zsh

set -eu # makes your program exit on error or unbound variable

progname=$0

function usage {
    echo "usage: $(basename $progname) TODO:ENTER USAGE"
    exit 1
}

#############################
runedm=true
runiom=true
runfpm=true
runpdm=true
runrev=true
#############################

path_to_datasets="datasets/"
path_to_results="results/"

# set virtual cohort
ln -fs "${path_to_datasets}"virtual_trial_cohort_1.csv virtual_cohort.csv

odexp -d codes/dynamical_models.pop  # generate code
odexp -c codes/dynamical_models.pop  # compile code

echo "opt loudness quiet" >>odexpdir/parameters.pop

# range over carbs, g_ED
cohort_size=1000
par1name="carbs"
par2name="g_ED"
par2=0.0
par1step=40.0
par2step=1
n1runs=3
n2runs=2
# Cohort 1 are relatively weight stable
sed -E -i .bak 's/(par[ ]+cohort_nbr[ ]+)[^ ]+/\11/' odexpdir/parameters.pop
sed -E -i .bak 's/(par[ ]+rhoEI1[ ]+)[^ ]+/\10.0/' odexpdir/parameters.pop
sed -E -i .bak 's/(times[ ].*$)/times 0 1 7 15 31 61/' odexpdir/parameters.pop
sed -E -i .bak 's/(opt[ ]+res[ ].*$)/opt res 2/' odexpdir/parameters.pop
sed -E -i .bak 's/(opt[ ]+ps[ ].*$)/opt ps 1000/' odexpdir/parameters.pop
echo -n "MODEL,VARIANT,$par1name,$par2name," >cohort_1_short_CIM_trial.csv
cp codes/traj_varnames.txt odexpdir/.
sed -E 's/\t/,/g' odexpdir/traj_varnames.txt >>cohort_1_short_CIM_trial.csv

coll_st=(0 1 3 5 7 9)

# CIM:EDM energy deposition model 
if [ "$runedm" = true ]
then
  echo "running short trial cohort 1, CIM:EDM"
  rm -f trial_cohort_1_edm.csv
  sed -E -i .bak 's/(par[ ]+MODEL[ ]+)[^ ]+/\11.0/' odexpdir/parameters.pop
  for n2 in {1..$n2runs} 
  do
    par1=10.0
    for n1 in {1..$n1runs}
    do
      echo "CIM:EDM; $par1name: $par1; $par2name: $par2"
      # set parameters
      sed -E -i .bak 's/(par[ ]+'"$par1name"'[ ]+)[^ ]+/\1'"$par1"'/' odexpdir/parameters.pop
      sed -E -i .bak 's/(par[ ]+'"$par2name"'[ ]+)[^ ]+/\1'"$par2"'/' odexpdir/parameters.pop
      ./model.out >/dev/null # run code

      for st in "${coll_st[@]}" 
      do
        for i in {1..$cohort_size}
        do
            echo "CIM,EDM,$par1,$par2," >>temp_pars.csv
        done
        hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' -v step=$st '$1 == step { print }' >temp_traj.csv
        lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
        cat temp_par_traj.csv >>!trial_cohort_1_edm.csv
        rm temp_pars.csv
      done 

      par1=$(($par1+$par1step))
      rm temp_par_traj.csv
      rm temp_traj.csv
    done

    par2=$(($par2+$par2step))
  done
fi # runcim
cat trial_cohort_1_edm.csv >>cohort_1_short_CIM_trial.csv

# CIM:APES anatomical partitioning of energy storage 
if [ "$runfpm" = true ]
then
  echo "running short trial cohort 1, CIM:APES"
  rm -f trial_cohort_1_fpm.csv
  sed -E -i .bak 's/(par[ ]+MODEL[ ]+)[^ ]+/\12.0/' odexpdir/parameters.pop
  par2=0.0
  for n2 in {1..$n2runs} 
  do
    par1=10.0
    for n1 in {1..$n1runs}
    do
      echo "CIM:APES; $par1name: $par1; $par2name: $par2"
      # set parameters
      sed -E -i .bak 's/(par[ ]+'"$par1name"'[ ]+)[^ ]+/\1'"$par1"'/' odexpdir/parameters.pop
      sed -E -i .bak 's/(par[ ]+'"$par2name"'[ ]+)[^ ]+/\1'"$par2"'/' odexpdir/parameters.pop
      ./model.out >/dev/null # run code

      for st in "${coll_st[@]}" 
      do
        for i in {1..$cohort_size}
        do
            echo "CIM,APES,$par1,$par2," >>temp_pars.csv
        done
        hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' -v step=$st '$1 == step { print }' >temp_traj.csv
        lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
        cat temp_par_traj.csv >>!trial_cohort_1_fpm.csv
        rm temp_pars.csv
      done 

      par1=$(($par1+$par1step))
      rm temp_par_traj.csv
      rm temp_traj.csv
    done

    par2=$(($par2+$par2step))
  done
fi # runfpm
cat trial_cohort_1_fpm.csv >>cohort_1_short_CIM_trial.csv

# CIM:PDM fuel partition/energy deposition model 
if [ "$runpdm" = true ]
then
  echo "running short trial cohort 1, CIM:PDM"
  rm -f trial_cohort_1_pdm.csv
  sed -E -i .bak 's/(par[ ]+MODEL[ ]+)[^ ]+/\13.0/' odexpdir/parameters.pop
  par2=0.0
  for n2 in {1..$n2runs} 
  do
    par1=10.0
    for n1 in {1..$n1runs}
    do
      echo "CIM:PDM; $par1name: $par1; $par2name: $par2"
      # set parameters
      sed -E -i .bak 's/(par[ ]+'"$par1name"'[ ]+)[^ ]+/\1'"$par1"'/' odexpdir/parameters.pop
      sed -E -i .bak 's/(par[ ]+'"$par2name"'[ ]+)[^ ]+/\1'"$par2"'/' odexpdir/parameters.pop
      ./model.out >/dev/null # run code

      for st in "${coll_st[@]}" 
      do
        for i in {1..$cohort_size}
        do
            echo "CIM,PDM,$par1,$par2," >>temp_pars.csv
        done
        hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' -v step=$st '$1 == step { print }' >temp_traj.csv
        lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
        cat temp_par_traj.csv >>!trial_cohort_1_pdm.csv
        rm temp_pars.csv
      done 

      par1=$(($par1+$par1step))
      rm temp_par_traj.csv
      rm temp_traj.csv
    done

    par2=$(($par2+$par2step))
  done
fi # runpdm
cat trial_cohort_1_pdm.csv >>cohort_1_short_CIM_trial.csv

# EBM 
if [ "$runiom" = true ]
then
  echo "running short trial cohort 1, EBM:IOM"
  rm -f trial_cohort_1_iom.csv
  sed -E -i .bak 's/(par[ ]+MODEL[ ]+)[^ ]+/\10.0/' odexpdir/parameters.pop
  par1=50.0
  par2=0.0
  echo "EBM; $par1name: $par1; $par2name: $par2"
  # set parameters
  sed -E -i .bak 's/(par[ ]+'"$par1name"'[ ]+)[^ ]+/\1'"$par1"'/' odexpdir/parameters.pop
  sed -E -i .bak 's/(par[ ]+'"$par2name"'[ ]+)[^ ]+/\1'"$par2"'/' odexpdir/parameters.pop
  ./model.out >/dev/null # run code

  for st in "${coll_st[@]}" 
  do
    for i in {1..$cohort_size}
    do
        echo "EBM,IOM,$par1,$par2," >>temp_pars.csv
    done
    hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' -v step=$st '$1 == step { print }' >temp_traj.csv
    lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
    cat temp_par_traj.csv >>!trial_cohort_1_iom.csv
    rm temp_par_traj.csv
    rm temp_pars.csv
    rm temp_traj.csv
  done 
fi # runiom
cat trial_cohort_1_iom.csv >>cohort_1_short_CIM_trial.csv

# CIM REVERSE MODEL (EDM - FFLOOP, g_ED = 1)
if [ "$runrev" = true ]
then
  echo "running short trial cohort 1, CIM:REV"
  rm -f trial_cohort_1_rev.csv
  sed -E -i .bak 's/(par[ ]+MODEL[ ]+)[^ ]+/\14.0/' odexpdir/parameters.pop
  par2=1.0
  par1=10.0
  for n1 in {1..$n1runs}
  do
    echo "CIM:REV; $par1name: $par1; $par2name: $par2"
    # set parameters
    sed -E -i .bak 's/(par[ ]+'"$par1name"'[ ]+)[^ ]+/\1'"$par1"'/' odexpdir/parameters.pop
    sed -E -i .bak 's/(par[ ]+'"$par2name"'[ ]+)[^ ]+/\1'"$par2"'/' odexpdir/parameters.pop
    ./model.out >/dev/null # run code

    for st in "${coll_st[@]}" 
    do
      for i in {1..$cohort_size}
      do
          echo "CIM,REV,$par1,$par2," >>temp_pars.csv
      done
      hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' -v step=$st '$1 == step { print }' >temp_traj.csv
      lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
      cat temp_par_traj.csv >>!trial_cohort_1_rev.csv
      rm temp_pars.csv
    done 

    par1=$(($par1+$par1step))
    rm temp_par_traj.csv
    rm temp_traj.csv
  done
fi # runrev
cat trial_cohort_1_rev.csv >>cohort_1_short_CIM_trial.csv


rm virtual_cohort.csv

mv cohort_1_short_CIM_trial.csv "${path_to_results}cohort_1_short_CIM_trial.csv"

exit 0
