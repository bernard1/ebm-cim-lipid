/* file main.h */

/* includes */
#include <gsl/gsl_vector.h>

#include "methods_odexp.h"
#include "gplot.h"

/* size of history buffer */
#define SIZEHIST 100

/* size of message buffer */
#define SIZEMSG  10000

enum plotmode { PM_UNDEFINED, PM_NORMAL, PM_FUN, 
  PM_ANIMATE, PM_CONTINUATION, PM_PARTICLES, PM_CURVES };

#define SS_NULL             0x0
#define SS_MODIFIED_RUN     0x01
#define SS_MODIFIED_PLOT    0x02
#define SS_ACTION_QUIT      0x04
#define SS_ACTION_RUN       0x08
#define SS_ACTION_PLOT      0x10
#define SS_ACTION_DATA      0x20


/* function declaration */
int odexp( oderhs pop_ode_rhs, oderhs single_rhs, odeic ode_ic, odeic single_ic, rootrhs root_rhs, const char *odexp_filename );

int setup_nve(const char fname[], const char key[], int keylen, char type, int prefix, nve *x);
int update_act_par_index(int *p, const nve mu);
int update_act_par_options(const int p, const nve mu);
int update_curves(int *nbr_hold, char plotkeys[100][EXPRLENGTH]);
int check_options( void );




/* readline */
void initialize_readline(void);
/* readline completion list */
char **completion_list_completion(const char *, int, int);
char *completion_list_generator(const char *, int);

