/* file datastruct.c */

/* includes */
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "datastruct.h"


const char PALETTE_ACID[8][7] = {"#002313", "#0000cc", "#cc0000", "#00cc00", "#cccc00", "#00cccc", "#cc00cc", "#cccccc"};
const char PALETTE_QUAL[9][7] = {"#1E659F", "#CB0002", "#339631", "#7F358A", "#E66600", "#E6E61A", "#8D3D0E", "#DE68A6", "#808080"};
const char PALETTE_APPLE[8][7] = {"#143d9d", "#dc143c", "#0c987d", "#ffd700", "#6eafc6", "#e34262", "#14de14", "#fff5c0"};
const char PALETTE_MONO[1][7] = {"#143d9d"};


int set_dou(const char *name, const double val) 
{
    int idx_opt = 0;
    while ( idx_opt < NBROPTS)
    {
        if ( strcmp(name, GOPTS[idx_opt].name) == 0 ) /* found option */
        {
          GOPTS[idx_opt].numval = val;
          return 0;
        }
        idx_opt++;
    }

    /* option not found */
    PRINTERR("  Error: Could not assign option %s",name);
    return 1;
}

int set_int(const char *name, const int val) 
{
    int idx_opt = 0;
    while ( idx_opt < NBROPTS)
    {
        if ( strcmp(name, GOPTS[idx_opt].name) == 0 ) /* found option */
        {
          GOPTS[idx_opt].intval = val;
          return 0;
        }
        idx_opt++;
    }

    /* option not found */
    PRINTERR("  Error: Could not assign option %s",name);
    return 1;
}

int set_str(const char *name, const char * val) 
{
    int idx_opt = 0;
    while ( idx_opt < NBROPTS)
    {
        if ( strcmp(name, GOPTS[idx_opt].name) == 0 ) /* found option */
        {
          strncpy(GOPTS[idx_opt].strval,val,NAMELENGTH);
          return 0;
        }
        idx_opt++;
    }

    /* option not found */
    PRINTERR("  Error: Could not assign option %s",name);
    return 1;
}


double get_dou(const char *name)
{
    int idx_opt = 0;
    while ( idx_opt < NBROPTS)
    {
        if ( strcmp(name, GOPTS[idx_opt].name) == 0 )
        {
            return GOPTS[idx_opt].numval;
        }
        idx_opt++;
    }
    return NAN;
}

int get_int(const char *name)
{
    int idx_opt = 0;
    while ( idx_opt < NBROPTS)
    {
        if ( strcmp(name, GOPTS[idx_opt].name) == 0 )
        {
            return GOPTS[idx_opt].intval;
        }
        idx_opt++;
    }
    return 0;
}


char * get_str(const char *name)
{
    int idx_opt = 0;
    while ( idx_opt < NBROPTS)
    {
        if ( strcmp(name, GOPTS[idx_opt].name) == 0 )
        {
            return GOPTS[idx_opt].strval;
        }
        idx_opt++;
    }
    return NULL;
}

int name2index( const char *name, nve var, int *n) /* get index of var.name == name */
{
    int i = 0;

    if ( (strcmp(name,"T") == 0) | (strcmp(name,"t") == 0) )
    {
        *n = -1;
        return 0;
    }
    while (  i < var.nbr_el )
    {
        if ( strcmp(name, var.name[i]) == 0 )
        {
          *n =  i;
          return 0;
        }
        i++;
    }

    /* else do not change *n */
    PRINTERR("  Error: Unknown variable/parameter name '%s'. List variables with 'lx' or 'lp'.",name);
    return 1;

}

int option_name2index( const char *name, int *n) /* get index of option.name == name or option.abbr == name */
{
    int i = 0;

    while (  i < NBROPTS )
    {
        if ( strcmp(name, GOPTS[i].name) == 0 ||  strcmp(name, GOPTS[i].abbr) == 0  )
        {
          *n =  i;
          return 1;
        }
        i++;
    }

    /* else do not change *n */
    PRINTERR("  Error: Unknown option '%s'",name);
    return 0;
}

void alloc_namevalexp( nve *var )
{
    int i;
    var->value = malloc(var->nbr_el*sizeof(double));
    var->name = malloc(var->nbr_el*sizeof(char*));
    var->expression = malloc(var->nbr_el*sizeof(char*));
    var->attribute = malloc(var->nbr_el*sizeof(char*));
    var->comment = malloc(var->nbr_el*sizeof(char*));
    var->expr_index = malloc(var->nbr_el*sizeof(int));
    var->max_name_length = malloc(sizeof(int));
    for (i = 0; i < var->nbr_el; i++)
    {
        var->name[i] = malloc(NAMELENGTH*sizeof(char));
        var->expression[i] = malloc(EXPRLENGTH*sizeof(char));
        var->attribute[i] = malloc(EXPRLENGTH*sizeof(char));
        var->comment[i] = malloc(EXPRLENGTH*sizeof(char));
    }
}

void free_namevalexp(nve var )
{
    int i;
    
    /* do not free _pointeris, they will be freed in time */
    for (i = 0; i < var.nbr_el; i++)
    {
        free(var.name[i]);
        free(var.expression[i]);
        free(var.attribute[i]);
        free(var.comment[i]);
    }
    free(var.value);
    free(var.name);
    free(var.expression);
    free(var.attribute);
    free(var.comment);
    free(var.expr_index);
    free(var.max_name_length);
}

/* init population of particles */
void init_dlist(dlist *list)
{
    list->start = NULL;
    list->end  = NULL;
    list->size = 0;
}

void init_world( world *s, nve *pex, nve *func, nve *mu,\
        nve *ics, nve *fcn, nve *eqn, nve *psi, nve *mfd,\
        nve *dxv, nve *cst, nve *dfl, int (*ode_rhs)(double, const double *, double *, void *))
{
    int i;

    s->nbr_par = mu->nbr_el;
    s->nbr_var = ics->nbr_el;
    s->nbr_expr= pex->nbr_el;
    s->nbr_aux = fcn->nbr_el;
    s->nbr_psi = psi->nbr_el;
    s->nbr_mfd = mfd->nbr_el;
    s->nbr_col = 1 + ics->nbr_el + fcn->nbr_el + psi->nbr_el + mfd->nbr_el + pex->nbr_el;
    s->parnames = mu->name;
    s->varnames = ics->name;
    s->exprnames= pex->name;
    s->auxnames = fcn->name;
    s->psinames = psi->name;
    s->mfdnames = mfd->name;
    
    s->pex_ptr = pex;     /* parametric expressions */
    s->func_ptr= func;    /* user-defined functions */
    s->mu_ptr  = mu;      /* parameters */
    s->ics_ptr = ics;     /* initial conditions */
    s->fcn_ptr = fcn;     /* auxiliary functions */
    s->eqn_ptr = eqn;;    /* dynamical equations */
    s->psi_ptr = psi;     /* pop coupling terms */
    s->mfd_ptr = mfd;     /* pop mean fields/stats */
    s->dxv_ptr = dxv;     /* list of all Dynamical  + auXiliary Variables */
    s->cst_ptr = cst;     /* constant arrays */
    s->dfl_ptr = dfl;     /* data files */

    s->mu = mu->value;
    s->meanfield = mfd->value;

    s->max_id = 0;

    s->event[0] = -1;
    s->event[1] =  1;
    s->event[2] =  0;

    s->stop_flag = 0;
    s->first_eval_flag = 0;

    s->time_in_ode_rhs = 0.0;
    s->ode_rhs = ode_rhs;
    s->nbrsteps = 0;

    /* write names of variables in separate file
     * stats.dat:
     *  time
     *  mean fields
     *  parent id
     *  birth death
     *  child id
     */
    if ( ( s->fstats_varnames = fopen(STATVAR_FILENAME, "w") ) == NULL )
    {
      PRINTERR("error: could not open file '" STATVAR_FILENAME "', exiting...\n");
      exit ( EXIT_FAILURE );
    }

    fprintf(s->fstats_varnames,"TIME");
    for(i=0; i<s->nbr_mfd; i++)
    {
       fprintf(s->fstats_varnames,"\t%s",s->mfdnames[i]);
    }
    fprintf(s->fstats_varnames,"\tN\tSTOP\tPARENT_ID\tEVENT\tCHILD_ID\n");
    fclose(s->fstats_varnames); 

    if ( ( s->ftraj_varnames = fopen(TRAJVAR_FILENAME, "w") ) == NULL )
    {
      PRINTERR("error: could not open file '" TRAJVAR_FILENAME "', exiting...\n");
      exit ( EXIT_FAILURE );
    }
    fprintf(s->fstats_varnames,"STEP\tID\tTIME");
    for(i=0; i<s->nbr_var; i++)
    {
       fprintf(s->ftraj_varnames,"\t%s",s->varnames[i]);
    }
    for(i=0; i<s->nbr_aux; i++)
    {
       fprintf(s->ftraj_varnames,"\t%s",s->auxnames[i]);
    }
    for(i=0; i<s->nbr_psi; i++)
    {
       fprintf(s->ftraj_varnames,"\t%s",s->psinames[i]);
    }
    for(i=0; i<s->nbr_mfd; i++)
    {
       fprintf(s->ftraj_varnames,"\t%s",s->mfdnames[i]);
    }
    for(i=0; i<s->nbr_expr; i++)
    {
       fprintf(s->ftraj_varnames,"\t%s",s->exprnames[i]);
    }
    fprintf(s->ftraj_varnames,"\n");
    fclose(s->ftraj_varnames); 

    s->pop = malloc(sizeof(dlist));
    init_dlist(s->pop);
}

int insert_endoflist( dlist *list, par *p )
{
    p->nextel = NULL; /* p is at the end of the list */
    p->prevel = list->end;
    if ( list->size > 0) /* list->end points to a cell */
    {
      list->end->nextel = p;
    }
    else /* there are no cells in the list, list->end points to NULL */ 
    {
      list->start = p;
    }
    list->end = p;
    list->size++;
    return 0;
}

/* insert at the end of the list (including an empty list) */
int par_birth ( void )
{
    par *p = malloc(sizeof(par));
    int i;
    p->nbr_expr = SIM->pex_ptr->nbr_el;
    p->nbr_aux  = SIM->fcn_ptr->nbr_el;
    p->nbr_y    = SIM->ics_ptr->nbr_el;
    p->nbr_psi  = SIM->psi_ptr->nbr_el;
    p->expr     = malloc(p->nbr_expr*sizeof(double));
    p->aux      = malloc(p->nbr_aux*sizeof(double));
    p->y        = malloc(p->nbr_y*sizeof(double));
    p->psi      = malloc(p->nbr_psi*sizeof(double));
    p->id       = SIM->max_id++;

    for (i=0;i<p->nbr_expr;i++)
    {
        p->expr[i] = SIM->pex_ptr->value[i];
    }
    for (i=0;i<p->nbr_aux;i++)
    {
        p->aux[i]  = SIM->fcn_ptr->value[i];
    }
    for (i=0;i<p->nbr_y;i++)
    {
        p->y[i]    = SIM->ics_ptr->value[i];
    }
    for (i=0;i<p->nbr_psi;i++)
    {
        p->psi[i]    = 0.0;
    }

    p->death_rate = 0.0;
    p->repli_rate = 0.0;

    p->sister = NULL; /* particle has no sister */

    insert_endoflist( SIM->pop, p );


    return 0;
}


int par_repli (par *mother)
{
    par *p = malloc(sizeof(par));
    int i;
    p->nbr_expr = mother->nbr_expr;
    p->nbr_aux  = mother->nbr_aux;
    p->nbr_y    = mother->nbr_y;
    p->nbr_psi  = mother->nbr_psi;
    p->expr     = malloc(p->nbr_expr*sizeof(double));
    p->aux      = malloc(p->nbr_aux*sizeof(double));
    p->y        = malloc(p->nbr_y*sizeof(double));
    p->psi      = malloc(p->nbr_psi*sizeof(double));
    p->id       = SIM->max_id++;

    for (i=0;i<p->nbr_expr;i++)
    {
        p->expr[i] = mother->expr[i];
    }
    for (i=0;i<p->nbr_aux;i++)
    {
        p->aux[i]  = mother->aux[i];
    }
    for (i=0;i<p->nbr_y;i++)
    {
        p->y[i]    = mother->y[i];
    }
    for (i=0;i<p->nbr_psi;i++)
    {
        p->psi[i]  = mother->psi[i]; 
    }
    p->death_rate = mother->death_rate;
    p->repli_rate = mother->repli_rate;

    p->sister = mother; /* pointer to mother particle. 
                         * Warning: existence of mother not guarateed 
                         * p->sister is reset to NULL after replication
                         * */

    insert_endoflist( SIM->pop, p );

    return 0;
}


/* delete element to_del from the list */
int delete_el( dlist *list, par *to_del)
{

    if (list->size == 0)
        return -1;

    if ( to_del->nextel != NULL && to_del->prevel != NULL) /* to_del is in the middle */
    {
        /* printf("deleting middle element of %d\n", cellList->size); */
        to_del->nextel->prevel = to_del->prevel;
        to_del->prevel->nextel = to_del->nextel;
    }
    else if ( to_del->nextel == NULL && to_del->prevel != NULL) /* to_del is last of many */
    {
        /* printf("deleting last of %d\n", cellList->size); */
        to_del->prevel->nextel = NULL;
        list->end = to_del->prevel;
    }
    else if ( to_del->nextel != NULL && to_del->prevel == NULL) /* to_del is first of many */
    {
        /* printf("deleting first of %d\n", cellList->size); */
        to_del->nextel->prevel = NULL;
        list->start = to_del->nextel;
    }
    else if ( to_del->nextel == NULL && to_del->prevel == NULL) /* to_del is the only cell */
    {
        /* printf("deleting last element\n"); */
        list->start = NULL;
        list->end = NULL;
    }

    free(to_del->expr);
    free(to_del->aux);
    free(to_del->y);
    free(to_del->psi);

    free(to_del);
    list->size--;

    return 0;

}

/* destroy the list */
void destroy(dlist *list)
{
    par *p, *next_p;
    p = list->start;
    while(p != NULL)
    {
        next_p = p->nextel;
        delete_el(list,p);
        p = next_p;
    }
    free( list );
}


void free_world(world *s)
{
    destroy(s->pop);
    free(s);
}


void printf_particle(par *p)
{
    int i;
    printf("-\n");
    printf("  particle         : " T_VAL "%d" T_NOR "\n",p->id);
    if ( p->nbr_y )
    {
      printf("  variables:\n");
    }
    for (i=0;i<p->nbr_y;i++)
    {
        printf("    %s%*s: " T_VAL "%g" T_NOR "\n",SIM->varnames[i], 15 - mbstrlen(SIM->varnames[i]), "",p->y[i]);
    }
    if ( p->nbr_aux )
    {
      printf("  auxiliary_terms:\n");
    }
    for (i=0;i<p->nbr_aux;i++)
    {
        printf("    %s%*s: " T_VAL "%g" T_NOR "\n",SIM->auxnames[i], 15 - mbstrlen(SIM->auxnames[i]), "", p->aux[i]);
    }
    if ( p->nbr_expr )
    {
      printf("  parametric_expressions:\n");
    }
    for (i=0;i<p->nbr_expr;i++)
    {
        printf("     %s%*s: " T_VAL "%g" T_NOR "\n",SIM->exprnames[i], 15 - mbstrlen(SIM->exprnames[i]), "", p->expr[i]);
    }
    if ( p->nbr_psi )
    {
      printf("  coupling_terms:\n");
    }
    for (i=0;i<p->nbr_psi;i++)
    {
        printf("     %s%*s: " T_VAL "%g" T_NOR "\n",SIM->psinames[i], 15 - mbstrlen(SIM->psinames[i]), "", p->psi[i]);
    }
    printf("  death_rate       : " T_VAL "%g" T_NOR "\n",p->death_rate);
    printf("  replication_rate : " T_VAL "%g" T_NOR "\n",p->repli_rate);

}


double getv(char *name, par *p)
{
    static int index = 0;
    while ( strncmp(name, SIM->auxnames[index], NAMELENGTH) ) 
    {                                                      
        index++; index %= SIM->nbr_aux;                    
    }                                                 
    return p->aux[index];                              
}

par * getpar( int with_id )
{
    par *pars = SIM->pop->start;
    while ( pars != NULL )
    {
        if ( pars->id == with_id )
            break;
        pars = pars->nextel;
    }
    if ( pars == NULL )
    {
        return (par *)NULL;
    }
    else
    {
        return pars;
    }
}

int fwrite_SIM(const double *restrict t) /* stats.dat file */
{
  /* update SIM file */
  fwrite(t,sizeof(double),1,SIM->fid);
  fwrite(SIM->meanfield,sizeof(double),SIM->nbr_mfd,SIM->fid);
  fwrite(&(SIM->pop->size),sizeof(int),1,SIM->fid);
  fwrite(&(EVENT_TYPE),sizeof(int),1,SIM->fid);
  fwrite(SIM->event,sizeof(int),3,SIM->fid);

  return 0;
}

int fwrite_all_particles(const double *restrict t)
{
    par *pars = SIM->pop->start;
    while ( pars != NULL )
    {
        fwrite(&(SIM->nbrsteps),sizeof(unsigned int),1,SIM->ftrajectories);
        fwrite(&(pars->id),sizeof(int),1,SIM->ftrajectories);
        fwrite(t,sizeof(double),1,SIM->ftrajectories);
        fwrite(pars->y,sizeof(double),pars->nbr_y,SIM->ftrajectories);
        fwrite(pars->aux,sizeof(double),pars->nbr_aux,SIM->ftrajectories);
        fwrite(pars->psi,sizeof(double),pars->nbr_psi,SIM->ftrajectories);
        fwrite(SIM->meanfield,sizeof(double),SIM->nbr_mfd,SIM->ftrajectories);
        fwrite(pars->expr,sizeof(double),pars->nbr_expr,SIM->ftrajectories);
        pars = pars->nextel;
    }
    (SIM->nbrsteps)++;
    
    return 0; 
}


int list_particle(int with_id)
{
    int fix = get_int("fix");
    int nbr_col = SIM->nbr_col; 
    char cmd_varnames[EXPRLENGTH];
    char cmd_data[EXPRLENGTH];
    char cmd_print[EXPRLENGTH];
    if ( with_id == -1 )
    {
      snprintf(cmd_varnames,EXPRLENGTH,"echo 'ID     ' | lam - " TRAJVAR_FILENAME " > " ODEXPDIR "id%d.txt", with_id);
      snprintf(cmd_data,EXPRLENGTH,\
            "hexdump -e '\"%%d \" %d \"%%5.%df\t\" \"\\n\"' " TRAJ_FILENAME " >> " ODEXPDIR "id%d.txt",\
            nbr_col, fix, with_id);
    }
    else
    {
      generate_particle_file(with_id); /* generate idXX.dat file for the current particle */
      snprintf(cmd_varnames,EXPRLENGTH,"cat " TRAJVAR_FILENAME " > " ODEXPDIR "id%d.txt", with_id);
      snprintf(cmd_data,EXPRLENGTH,\
            "hexdump -e '%d \"%%5.%df\t\" \"\\n\"' " ODEXPDIR "id%d.dat >> " ODEXPDIR "id%d.txt",\
            nbr_col, fix, with_id, with_id);
    } 
    snprintf(cmd_print,EXPRLENGTH,"column -t " ODEXPDIR "id%d.txt | less -sS", with_id);
    system(cmd_varnames); 
    system(cmd_data); 
    system(cmd_print);

    return 0;
}

int list_stats( void )
{
    int fix = get_int("fix");
    int nbr_col = 1 + SIM->nbr_mfd; 
    char cmd_varnames[EXPRLENGTH];
    char cmd_data[EXPRLENGTH];
    snprintf(cmd_varnames,EXPRLENGTH,"cat " STATVAR_FILENAME " > " ODEXPDIR "stats.csv");
    snprintf(cmd_data,EXPRLENGTH,\
            "hexdump -e '%d \"%%5.%df\t\" \"\t\" 5 \"%%d\t\" \"\\n\"' " STATS_FILENAME " >> " ODEXPDIR "stats.csv",\
            nbr_col, fix);

    system(cmd_varnames); 
    system(cmd_data); 
    system("column -t " ODEXPDIR "stats.csv | less -sS");

    return 0;
}


int list_traj( void )
{
    int fix = get_int("fix");
    int nbr_col = SIM->nbr_col;
    char cmd_varnames[EXPRLENGTH];
    char cmd_data[EXPRLENGTH];
    snprintf(cmd_varnames,EXPRLENGTH,"echo 'STEP     ID     ' | paste - " TRAJVAR_FILENAME " > " ODEXPDIR "traj.csv");
    snprintf(cmd_data,EXPRLENGTH,\
            "hexdump -e '\"%%u \" \"%%d\t\" %d \"%%5.%df\t\" \"\\n\"' " TRAJ_FILENAME " >> " ODEXPDIR "traj.csv",
            nbr_col, fix);

    system(cmd_varnames); 
    system(cmd_data); 
    system("column -t " ODEXPDIR "traj.csv | less -sS");

    return 0;
}

int generate_particle_file(int with_id)
{
  FILE *fin = NULL;
  FILE *fout = NULL;
  int id,st;
  double *row = NULL;
  char filename[MAXFILENAMELENGTH];
  int nbr_col = SIM->nbr_col; 
  row = malloc(nbr_col*sizeof(double));
  if ( ( fin = fopen(TRAJ_FILENAME, "r") ) == NULL )
  {
    PRINTERR("error: could not open file '" TRAJ_FILENAME "'.\n");
    return 1;
  }
  snprintf(filename,MAXFILENAMELENGTH, ODEXPDIR "id%d.dat", with_id);
  if ( ( fout = fopen(filename, "w") ) == NULL )
  {
    PRINTERR("error: could not open file '%s'.\n", filename);
    return 1;
  }

  while ( fread(&st,sizeof(int),1,fin) )
  {
    fread(&id,sizeof(int),1,fin);
    fread(row,sizeof(double),nbr_col,fin);
    if ( id == with_id ) 
    {
      fwrite(row,sizeof(double),nbr_col,fout);
    }
  }
  fclose(fin);
  fclose(fout);
  free(row);
  return 0;
}


int generate_particle_states()
{
  FILE *fin = NULL;
  FILE *fout = NULL;
  unsigned int id,st;
  double *row = NULL;
  int nbr_col = SIM->nbr_col; 
  static unsigned int timestep = 0;
  char key[256];

  double t = 0.0;
  int jump = 1;

  row = malloc(nbr_col*sizeof(double));

  /* select particle plot time step */ 
  if ( strncmp(get_str("timeframe"),"first", 5) == 0 )
  {
    timestep = 0;
  }
  else if ( strncmp(get_str("timeframe"),"last", 4) == 0 )
  {
    timestep = SIM->nbrsteps-1;
  }
  else if ( strncmp(get_str("timeframe"),"next", 4) == 0 )
  {
    sscanf(get_str("timeframe"),"%*s %u",&jump); 
    timestep += jump;
  }
  else if ( strncmp(get_str("timeframe"),"previous", 8) == 0 )
  {
    sscanf(get_str("timeframe"),"%*s %u",&jump); 
    timestep -= jump;
  }
  else if ( strncmp(get_str("timeframe"),"frame", 5) == 0 )
  {
    sscanf(get_str("timeframe"),"%*s %u",&timestep); 
  }
  else
  {
    timestep = (unsigned int)strtol(get_str("timeframe"),NULL,10);
  }
  
  timestep = max(timestep,(unsigned int)0);
  timestep = min(timestep,SIM->nbrsteps-1);

  if ( ( fin = fopen(TRAJ_FILENAME, "r") ) == NULL )
  {
    PRINTERR("error: could not open file '" TRAJ_FILENAME "'.\n");
    return 1;
  }
  if ( ( fout = fopen(PSTATE_FILENAME,"w") ) == NULL )
  {
    PRINTERR("error: could not open file '" PSTATE_FILENAME "'.\n");
    return 1;
  }

  while ( fread(&st,sizeof(unsigned int),1,fin)  )
  {
    fread(&id,sizeof(int),1,fin);
    fread(row,sizeof(double),nbr_col,fin);
    if ( st == timestep )
    {
      fwrite(&id,sizeof(int),1,fout);
      fwrite(row+1,sizeof(double),nbr_col-1,fout);
      t = row[0];
    }
  }
  printf("  t = %g (time frame %u)\n", t, timestep);
  sprintf(key,"%s=%8.2f (step %d)", get_str("indvar"), t, timestep);
  set_str("plotkey",key);
  fclose(fin);
  fclose(fout);
  free(row);
  return 0;
}


/* get the value of variable s, for particle p, into ptr */ 
#if 0
int mvar(const char *name, par *m, double *ptr)                        
{
  int index = 0;                                       
  while (  index < SIM->nbr_var )                       
  {                                                       
    if ( strncmp(name, SIM->varnames[index], NAMELENGTH) )
    {
     index++;
    }
    else
    {
      *ptr = m->y[index]; 
      return 0;
    }
  }                                                       
  index = 0;
  while (  index < SIM->nbr_aux )                       
  {                                                       
    if ( strncmp(name, SIM->auxnames[index], NAMELENGTH) )
    {
     index++;
    }
    else
    {
      *ptr = m->aux[index];
      return 0;
    }
  }                                                       
  index = 0;
  ptr = NULL;
  return 1;
}
#endif

void trim(char *str, const char c)
{
  char *first, *last;
  char *swap;
  int len;

  first = strchr(str,c);
  last  = strchr(str,'\0');

  if ( first == NULL ) /* then char c was not found, return */
    return;

  /* char c was found, search for last occurence */
  while ( *(--last) != c )
    ;

  if ( ( len = last - first ) > 0 )
  {
    swap = malloc(len*sizeof(char));
    strncpy(swap,first + 1,len - 1); 
    strncpy(str,swap,len - 1);
    str[len-1] = '\0';
    free(swap);
  }
  else 
  {
    /* do nothing */
  }

}

int mbstrlen(const char *string)
/* interpreted as UTF-8 string of multi-byte characters */
{
    const unsigned char *str_p = (unsigned char *)string;
    int len = 0;
    while ( *str_p != 0 ) 
    {
      len++;
      if ( *str_p < 0x80 ) 
      {
        str_p += 1;
      }
      else if ( (*str_p >= 0xC2) && (*str_p <= 0xDF) )
      {
        str_p += 2;
      }
      else if ( (*str_p >= 0xE0) && (*str_p <= 0xEF) )
      {
        str_p += 3;
      }
      else if ( (*str_p >= 0xF0) )
      {
        str_p += 4;
      }
      else 
      {
        str_p += 1;
      }
    }
    return len;

}

int get_attribute(const char *s, const char *key, char *val)
{
  int k = 0;
  int slen = strlen(s);
  int keylen = strlen(key);
  while ( k < slen )
  {
    if(strncmp(s + k,key,keylen)==0)
    {
      /* key found */
      if ( sscanf(s + k, "%*s = \"%[^\"]\"", val) )
      {
        /* val is between double-quotes "" */
        parse_quoted_string(s + k, '\"', val, NAMELENGTH);
      }
      else /* val is not between double-quotes, scan until space */
      {
        sscanf(s + k, "%*s = %s", val);
      }
      return 1;
    }
    k++;
  }
  return 0;
}


/* parse quoted string with escape characters */
int parse_quoted_string(const char *string, char quote, char *val, int len)
{
  char escape_char = '\\';
  int state = 0; /* 0: normal, 1: escaped */
  int i = 0;
  int j = 0;
  /* 1. ignore all blanks until first quote */
  while ( string[i] != 0 )
  {
    if ( string[i++] == quote )
    {
      break;
    }
  }
  /* 2. scan until non-escaped quote */
  while ( string[i] != 0 ) /* stop at null char */
  {
    if ( string[i] == escape_char && state == 0 )
    {
      state = 1;
      i++;
    }
    else if ( state == 1 || string[i] != quote ) /* write character */
    {
      if ( j > (len-2) )
      {
        break;
      }
      val[j] = string[i];
      state = 0;
      i++;
      j++;
    }
    else if ( string[i] == quote )
    {
      break;
    }
  }
  val[j] = 0; /* terminate with null character */
  
  return j;
}

