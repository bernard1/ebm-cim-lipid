/* arpnfun.c 
 * 
 * arpn construct an array of strings from basic array operations 
 *
 */

#include "arpnfun.h"

#define ISNUM(str) ( isdigit((char) *(str)) || ((char) *(str)) == '-' ) 

int literal = 0; /* bool: string between ' ' does not get expanded */
int expand_vars = 0; /* bool: expand tokens starting with $ */

func fcns[ARPN_NBR_FCNS] = {
  {"len",           fcn_length},
  {"ifelse",        fcn_ifelse},
  {"cat",           fcn_concatenate},
  {")",             fcn_concatenate},
  {"join",          fcn_join},
  {"quote",         fcn_quote},
  {"eval",          fcn_eval},
  {"reverse",       fcn_reverse},
  {"reshape",       fcn_reshape},
  {"rep",           fcn_replicate}, 
  {"add",           fcn_add},
  {"mult",          fcn_mult},
  {"power",         fcn_power},
  {"equal",         fcn_equal},
  {"neq",           fcn_neq}, 
  {"gt",            fcn_greaterthan},
  {"lt",            fcn_lessthan}, 
  {"find",          fcn_find}, 
  {"nth",           fcn_nth},
  {"def",           fcn_define},
  {"let",           fcn_let},
  {"dup",           fcn_duplicate},
  {"range",         fcn_range},
  {"timediff",      fcn_date}
};

keyval char_register[ARPN_NBR_CHAR_REGISTER] = {
  { 'A', "0" }, { 'B', "0" }, { 'C', "0" }, { 'D', "0" }, { 'E', "0" },
  { 'F', "0" }, { 'G', "0" }, { 'H', "0" }, { 'I', "0" }, { 'J', "0" },
  { 'K', "0" }, { 'L', "0" }, { 'M', "0" }, { 'N', "0" }, { 'O', "0" },
  { 'P', "0" }, { 'Q', "0" }, { 'R', "0" }, { 'S', "0" }, { 'T', "0" },
  { 'U', "0" }, { 'V', "0" }, { 'W', "0" }, { 'X', "0" }, { 'Y', "0" },
  { 'Z', "0" }
  };

/* expand token */
void expand_variable(char *val, const char *tok)
{
  int i = 0;
  /* first look for parameter with name $tok */
  while ( i < SIM->nbr_par ) 
  {
    if ( strcmp(SIM->parnames[i],tok+1) == 0 )
    { 
      sprintf(val,"%g",SIM->mu[i]);  
      return;
    }
    ++i;
  }

  /* then look in the char register */
  i = 0;
  while ( i < ARPN_NBR_CHAR_REGISTER ) 
  {
    if ( char_register[i].key == *(tok+1) )
    { 
      sprintf(val,"%s",char_register[i].val);  
      return;
    }
    ++i;
  }

  /* then look for builtin variables */
  if ( strcmp("popsize",tok+1) == 0 )
  {
    sprintf(val,"%d",POP_SIZE);  
  }

  if ( strcmp("nvar",tok+1) == 0 )
  {
    sprintf(val,"%d",SIM->nbr_var);  
  }

  if ( strcmp("rhstime",tok+1) == 0 )
  {
    sprintf(val,"%g msec",SIM->time_in_ode_rhs);  
  }
}

/* print the array a, whitespace */
void printa(str_array a)
{
  int i;
  for ( i = 0; i < a.length; ++i )
  {
    printf("%s ",a.array[i]);
  }
  /* printf(" )"); */
  printf("\n");
}

char* getnextt(const str_array a, int *n)
{
  char *val = NULL;
  if ( *n < a.length )
  {
    if ( *(a.array[*n]) != ';' )
    {
      if ( *(a.array[*n]) == '$' )
      {
        val = malloc(128*sizeof(char));
        expand_variable(val,a.array[*n]);
        a.array[*n] = realloc(a.array[*n], (strlen(val)+1)*sizeof(char));
        strcpy(a.array[*n],val);  
        free(val);
      }
      return a.array[(*n)++];
    }
  }
  return NULL;
}

int getnargs(const str_array a, int n)
{
  int nbr_args = 0;
  while ( (n + nbr_args) < a.length && *(a.array[n + nbr_args]) != ';' )
    nbr_args++;
  return nbr_args;
}

void joinargs(char chptr[], size_t cap, const str_array a, int *n, const char *sep)
 /* joinargs: joins the elements of str_array a starting at
  * element index pointed to by n into the string chptr[] separated by
  * the char array sep. Stops at end of str_array or a first occurence of ';'
  */
{
  char *tok;
  *chptr = '\0';
  while ( (tok = getnextt(a, n)) != NULL ) 
  {
    if (strlen(tok) + 1 > cap - strlen(chptr))
    {
      fprintf(stderr,"  token would be truncated\n");
    }
    else
    {
      (void)strncat(chptr, tok, cap - strlen(chptr) - 1);
    }
    if (strlen(sep) + 1 > cap - strlen(chptr))
    {
      fprintf(stderr,"  sep would be truncated\n");
    }
    else
    {
      (void)strncat(chptr, sep, cap - strlen(chptr) - 1);
    }
  } 
  chptr[strlen(chptr)-strlen(sep)] = '\0'; /* trim ending white space */
}

void join_elements(char chptr[], size_t cap, const str_array a, int *n, const char *sep)
 /* join_elements: joins the elements of str_array a starting at
  * element index pointed to by n into the string chptr[] separated by
  * the char array sep.
  */
{
  char *tok;
  *chptr = '\0';
  while ( *n < a.length )
  {
    tok = a.array[(*n)++];
    if (strlen(tok) + 1 > cap - strlen(chptr))
    {
      fprintf(stderr,"  token would be truncated\n");
    }
    else
    {
      (void)strncat(chptr, tok, cap - strlen(chptr) - 1);
    }
    if (strlen(sep) + 1 > cap - strlen(chptr))
    {
      fprintf(stderr,"  sep would be truncated\n");
    }
    else
    {
      (void)strncat(chptr, sep, cap - strlen(chptr) - 1);
    }
  } 
  chptr[strlen(chptr)-strlen(sep)] = '\0'; /* trim ending white space */
}

/* convert elements of a in an array of doubles */
void asnumerical(const str_array * restrict a, double **ax)
{
  int i;
  (*ax) = malloc(a->length*sizeof(double));
  for ( i = 0; i < a->length; ++i )
  {
    if ( ISNUM(a->array[i]) )
    { 
      (*ax)[i] = strtod(a->array[i],NULL);
    }
    else
    { 
      (*ax)[i] = NAN; 
    }
  }
}

void aschar(str_array *a, double *ax)
{
  int i;
  char s[32];
  for ( i = 0; i < a->length; ++i )
  {
    sprintf(s,"%g",ax[i]);
    a->array[i] = strdup(s);
  }
}

/* reverse the array a */
void rev(str_array *a)
{
  int i1 = 0, i9 = (a->length-1);
  char *swap;
  while ( i1 < i9 )
  {
    swap = a->array[i1];
    a->array[i1] = a->array[i9];
    a->array[i9] = swap;
    ++i1;
    --i9;
  }
}

/* append string newstr to array a */
void append(str_array *a, char *newstr)
{
  char *val = NULL;
  (a->length)++;
  a->array = realloc(a->array, a->length*sizeof(char*));
  if ( *newstr == '$' && (expand_vars == 1) )
  {
    val = malloc(128*sizeof(char));
    expand_variable(val,newstr);
    a->array[a->length-1] = malloc((strlen(val)+1)*sizeof(char));
    strcpy(a->array[a->length-1],val);  
    free(val);
  }
  else
  {
    a->array[a->length-1] = malloc((strlen(newstr)+1)*sizeof(char));
    strcpy(a->array[a->length-1],newstr);  
  }
}

/* concatenate two arrays a, b: (a, b) */
void cat(str_array *a, str_array *b)
{
  int i;
  /* copy */
  for ( i = 0; i < b->length; ++i )
  {
    append(a,b->array[i]);
  }
  free_array(b);
}

/* insert array ins into array a, at location loc */
void insert(str_array *a, const str_array *ins, int loc)
{
  int i;
  str_array b = {NULL, 0};  
  split(a,&b,loc);
  for ( i = 0; i < ins->length; ++i )
  {
    append(a, ins->array[i]);
  }
  cat(a,&b);
}

/* split array a into two arrays stored into a and b, at locoation loc
 * cat(a,b) restores the original array 
 * content of array b is destroyed */
void split(str_array *a, str_array *b, int loc)
{
  int i;
  free_array(b);
  b->array = NULL;
  if ( loc > a->length )
  {
    fprintf(stderr,"split: location %d larger than array length %d\n", loc, a->length);
  }
  for ( i = 0; i < (a->length - loc); ++i )
  {
    append(b,a->array[loc + i]);
    free(a->array[loc + i]);
  }
  a->length = loc;
  a->array = realloc(a->array, a->length*sizeof(char*));
}

/* delete element loc of array a */
void delete(str_array *a, int loc)
{
  int i;
  free(a->array[loc]);
  for ( i = 0; i < (a->length - loc - 1); ++i )
  {
    a->array[loc + i] = a->array[loc + i + 1]; 
  }
  (a->length)--;
  a->array = realloc(a->array, a->length*sizeof(char*));
}
    
void free_array(str_array *a)
{
  int i;
  for ( i = 0; i < a->length; ++i )
  {
    free(a->array[i]);
  }
  free(a->array);
  a->length = 0;
}

/* ifelse */
void fcn_ifelse(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0}, b2 = {NULL, 0}, b3 = {NULL, 0};
  int cond;
  b1 = arpn(&b1,ntok,tokens); /* condition  */
  b2 = arpn(&b2,ntok,tokens); /* if true */
  b3 = arpn(&b3,ntok,tokens); /* if false */
  if ( ISNUM(b1.array[0]) )
  {
    cond = (strtod(b1.array[0],NULL) != 0.0); 
  }
  else
  {
    cond = (strcmp(b1.array[0],"true") == 0 );
  }
  if ( cond )
  {
    free_array(&b3);
    cat(a,&b2);
  }
  else
  {
    free_array(&b2);
    cat(a,&b3);
  }
}
  

/* length of array */
void fcn_length(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0};
  unsigned int len;
  char s[20];
  b1 = arpn(&b1,ntok,tokens); 
  len = b1.length;
  free_array(&b1);
  b1.array = NULL;
  sprintf(s,"%u",len);
  append(&b1,s);
  cat(a,&b1);
}

void fcn_concatenate(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0}, b2 = {NULL, 0};
  b1 = arpn(&b1,ntok,tokens); /* left  */
  b2 = arpn(&b2,ntok,tokens); /* right */
  cat(&b1,&b2);
  cat(a,&b1);
}

void fcn_join(str_array *a, int *ntok, char *tokens[])
/* fcn_join: join elements of list 2 with separator in list 1 
 * list one must be of length 0 or 1. If length 0, no separator
 * is used. If length 1, is must may be a quoted string or
 * a string without spaces or delimiters
 */
{
  str_array b1 = {NULL, 0}, b2 = {NULL, 0};
  char sep[ARPN_EXPRESSION_LENGTH];
  char string[ARPN_EXPRESSION_LENGTH];
  int bntok = 0;
  b1 = arpn(&b1,ntok,tokens); 
  b2 = arpn(&b2,ntok,tokens); 
  rev(&b2);
  if ( b1.length == 1 )
  {
    if ( sscanf(b1.array[0],"\"%[^\"]\"",sep) == 1 )
    {
      join_elements(string, ARPN_EXPRESSION_LENGTH, b2, &bntok, sep);
    }
    else
    {
      join_elements(string, ARPN_EXPRESSION_LENGTH, b2, &bntok, b1.array[0]);
    }
  }
  else
  {
    join_elements(string, ARPN_EXPRESSION_LENGTH, b2, &bntok, "");
  }
  append(a,string);
}

void fcn_quote(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0};
  b1 = arpn(&b1,ntok,tokens); 
  char string[128];
  if ( b1.length != 1 )
  {
    fprintf(stderr,"quote: array not length 1: "
        "(%d)\n", b1.length);
  }
  string[0] = '\0';
  strcat(string,"\"");
  strncat(string,b1.array[0],125);
  strcat(string,"\"");
  free_array(&b1);
  append(a,string);
}

void fcn_eval(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0}, b2 = {NULL, 0};
  int bntok = 0;
  char expression[ARPN_EXPRESSION_LENGTH];
  expand_vars = 1;
  b1 = arpn(&b1,ntok,tokens); /* get remaining of the list */
  if ( b1.length == 1 )
  {
    b2 = repl(b1.array[0]);
  }
  else
  {
    rev(&b1);
    join_elements(expression, ARPN_EXPRESSION_LENGTH, b1, &bntok, " ");
    b2 = repl(expression);
    /* fprintf(stderr,"eval: array not length 1: "
        "(%d)\n", b1.length); 
    */
  }
  expand_vars = 0;
  free_array(&b1);
  rev(&b2);
  cat(a,&b2);
}

void fcn_reverse(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0};
  b1 = arpn(&b1,ntok,tokens);
  rev(&b1);
  cat(a,&b1);
}

void fcn_reshape(str_array *a, int *ntok, char *tokens[])
{
  str_array b = {NULL, 0}, r = {NULL, 0};
  str_array tmp = {NULL, 0};
  int *m, *mr, *mf;
  int i,j,len, d, p;
  r = arpn(&r,ntok,tokens); /* reshape */
  b = arpn(&b,ntok,tokens); /* array */
  len = b.length;
  d = r.length;
  m  = malloc((d+1)*sizeof(int));
  mr = malloc((d+1)*sizeof(int));
  mf = malloc((d+1)*sizeof(int));
  m[0] = 1;
  mr[0] = 1;
  mf[0] = 1;
  for ( j = 0; j < d; ++j )
  {
    m[j+1] = strtol(r.array[j],NULL,10); /* m = (1,r1,r2,r3,...) */
    if (  ( len % m[j+1] ) != 0 )
    {
      fprintf(stderr,"reshape: new dim %d "
                     "in not a divisor of array length %d\n", m[j+1],len);
      /* exit ( EXIT_FAILURE ); */
    }
    mr[j+1] = strtol(r.array[j],NULL,10)*mr[j];
    mf[j+1] = strtol(r.array[d - 1 - j],NULL,10)*mf[j];
  }
  for ( i = 0; i < len; ++i )
  {
    p = 0;
    for ( j = 0; j < d; ++j )
    {
      p += (( i/mr[j] ) % m[j+1])*mf[d - 1 - j]; 
    }
    append(&tmp,b.array[p]);
  }

  free(m);
  free(mr);
  free(mf);

  free_array(&r);
  free_array(&b);
  cat(a,&tmp);
}

void fcn_replicate(str_array *a, int *ntok, char *tokens[])
{
  str_array b = {NULL, 0}, r = {NULL, 0};
  int i,j;
  int repeat = 1;
  double add_step = 0.0;
  str_array tmp = {NULL, 0};
  r = arpn(&r,ntok,tokens); /* repeat */
  b = arpn(&b,ntok,tokens); /* array */
  switch(r.length)
  {
    case 1: 
      repeat = strtol(r.array[0],NULL,10);
      break;
    case 2:
      add_step = strtod(r.array[0],NULL);
      repeat = strtol(r.array[1],NULL,10);
      break;
  }
  rev(&b);
  /* replicate */
  for ( j = 0; j < repeat; ++j)
  {
    for ( i = 0; i < b.length; ++i )
    {
      append(&tmp, b.array[i]);
      if ( ISNUM(b.array[i]) && r.length == 2 )
      {  
        sprintf(b.array[i],"%g", strtod(b.array[i],NULL) + add_step); 
      }
    }
  }
  rev(&tmp);
  free_array(&r);
  free_array(&b);
  cat(a,&tmp);
}

void fcn_add(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0}, b2 = {NULL, 0};
  int i, len;
  double *x2;
  b1 = arpn(&b1,ntok,tokens); /* get next array */
  b2 = arpn(&b2,ntok,tokens); /* get next array */
  len = b2.length;
  asnumerical(&b2,&x2);
  free_array(&b2);
  b2.array = NULL;
  if ( b1.length == 0 && len > 0 )
  {
    /* apply add on elements of b2 */
    char str_sum[20];
    for ( i = 1; i < len; ++i )
    {
      x2[0] += x2[i];
    }
    sprintf(str_sum,"%g",x2[0]);
    free(x2);
    append(&b2,str_sum);
  }
  else if ( b1.length == 1 && len > 0 )
  {
    /* add b1 to elements of b2 */
    double *x1;
    char str_sum[20];
    asnumerical(&b1,&x1);
    for ( i = 0; i < len; ++i )
    {
      x2[i] += x1[0];
      sprintf(str_sum,"%g",x2[i]);
      append(&b2,str_sum);
    }
    free(x1);
    free(x2);
  }
  else if ( b1.length == len )
  {
    /* add the two arrays */
    double *x1;
    char str_sum[20];
    asnumerical(&b1,&x1);
    for ( i = 0; i < len; ++i )
    {
      x2[i] += x1[i];
      sprintf(str_sum,"%g",x2[i]);
      append(&b2,str_sum);
    }
    free(x1);
    free(x2);
  }
  else 
  {
    fprintf(stderr,"add: arrays are different lengths\n");
    /* exit( EXIT_FAILURE ); */
  }
  free_array(&b1);
  cat(a,&b2);
}

void fcn_mult(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0}, b2 = {NULL, 0};
  int i, len;
  double *x2;
  b1 = arpn(&b1,ntok,tokens); /* get next array */
  b2 = arpn(&b2,ntok,tokens); /* get next array */
  len = b2.length;
  asnumerical(&b2,&x2);
  free_array(&b2);
  b2.array = NULL;
  if ( b1.length == 0 && len > 0 )
  {
    /* apply add on elements of b2 */
    char str_sum[20];
    for ( i = 1; i < len; ++i )
    {
      x2[0] *= x2[i];
    }
    sprintf(str_sum,"%g",x2[0]);
    free(x2);
    append(&b2,str_sum);
  }
  else if ( b1.length == 1 && len > 0 )
  {
    /* add b1 to elements of b2 */
    double *x1;
    char str_sum[20];
    asnumerical(&b1,&x1);
    for ( i = 0; i < len; ++i )
    {
      x2[i] *= x1[0];
      sprintf(str_sum,"%g",x2[i]);
      append(&b2,str_sum);
    }
    free(x1);
    free(x2);
  }
  else if ( b1.length == len )
  {
    /* add the two arrays */
    double *x1;
    char str_sum[20];
    asnumerical(&b1,&x1);
    for ( i = 0; i < len; ++i )
    {
      x2[i] *= x1[i];
      sprintf(str_sum,"%g",x2[i]);
      append(&b2,str_sum);
    }
    free(x1);
    free(x2);
  }
  else 
  {
    fprintf(stderr,"add: arrays are different lengths\n");
    /* exit( EXIT_FAILURE ); */
  }
  free_array(&b1);
  cat(a,&b2);
}

void fcn_power(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0}, b2 = {NULL, 0};
  double *swap;
  double *x1, *x2;
  int i;
  b1 = arpn(&b1,ntok,tokens); /* exponent */
  b2 = arpn(&b2,ntok,tokens); /* base */
  asnumerical(&b1,&x1);
  asnumerical(&b2,&x2);
  if ( b1.length == 1 && b2.length > 0 )
  {
    /* power b1 to elements of b2 */
    for ( i = 0; i < b2.length; ++i )
    {
      x2[i] = pow(x2[i],x1[0]);
    }
  }
  else if ( b2.length == 1 && b1.length > 0 )
  {
    /* b2 to the power of elements of b1 */
    for ( i = 0; i < b1.length; ++i )
    {
      x1[i] = pow(x2[0],x1[i]);
    }
    swap = x2;
    x2 = x1;
    x1 = swap;
    b2.length = b1.length;
    b1.length = 1;
  }
  else if ( b1.length == b2.length )
  {
    /* power b2^b1 */
    for ( i = 0; i < b2.length; ++i )
    {
      x2[i] = pow(x2[i],x1[i]);
    }
  }
  else 
  {
    fprintf(stderr,"mult: arrays are different lengths\n");
    /* exit( EXIT_FAILURE ); */
  }
  aschar(&b2,x2);
  free_array(&b1);
  cat(a,&b2);
}

void fcn_equal(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0}, b2 = {NULL, 0};
  int i;
  b1 = arpn(&b1,ntok,tokens); /* first */
  b2 = arpn(&b2,ntok,tokens); /* second */
  if ( b1.length  == 0 && b2.length > 0 )
  {
    /* default equal to 0 */
    for ( i = 0; i < b2.length; ++i )
    {
      if ( ISNUM(b2.array[i]) )
      {
        b2.array[i] = strdup(strtod(b2.array[i],NULL) == 0 ? "1" : "0");
      }
      else
      {
        b2.array[i] = strdup(*b2.array[i] == '\0' ? "false" : "true");
      }
    }
  }
  else if ( b1.length == 1 && b2.length > 0 )
  {
    /* comp b1 to elements of b2 */
    for ( i = 0; i < b2.length; ++i )
    {
      if ( ISNUM(b2.array[i]) )
      {
        b2.array[i] = strdup(strtod(b2.array[i],NULL) == strtod(b1.array[0],NULL) ? \
            "1" : "0");
      }
      else
      {
        b2.array[i] = strdup(strcmp(b2.array[i],b1.array[0]) == 0 ? "true" : "false");
      }
    }
  }
  else if ( b1.length == b2.length )
  {
    /* element-wise equality  */
    for ( i = 0; i < b1.length; ++i )
    {
      if ( ISNUM(b2.array[i]) )
      {
        b2.array[i] = strdup(strtod(b2.array[i],NULL) == strtod(b1.array[i],NULL) ? \
            "1" : "0");
      }
      else
      {
        b2.array[i] = strdup(strcmp(b2.array[i],b1.array[i]) == 0 ? "true" : "false");
      }
    }
  }
  else
  {
    fprintf(stderr,"add: arrays are different lengths\n");
    /* exit( EXIT_FAILURE ); */
  }
  free_array(&b1);
  cat(a,&b2);
}

void fcn_neq(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0}, b2 = {NULL, 0};
  int i;
  b1 = arpn(&b1,ntok,tokens); /* first */
  b2 = arpn(&b2,ntok,tokens); /* second */
  if ( b1.length  == 0 && b2.length > 0 )
  {
    /* default equal to 0 */
    for ( i = 0; i < b2.length; ++i )
    {
      if ( ISNUM(b2.array[i]) )
      {
        b2.array[i] = strdup(strtod(b2.array[i],NULL) == 0 ? "0" : "1");
      }
      else
      {
        b2.array[i] = strdup(*b2.array[i] == '\0' ? "true" : "false");
      }
    }
  }
  else if ( b1.length == 1 && b2.length > 0 )
  {
    /* comp b1 to elements of b2 */
    for ( i = 0; i < b2.length; ++i )
    {
      if ( ISNUM(b2.array[i]) )
      {
        b2.array[i] = strdup(strtod(b2.array[i],NULL) == strtod(b1.array[0],NULL) ? \
            "0" : "1");
      }
      else
      {
        b2.array[i] = strdup(strcmp(b2.array[i],b1.array[0]) == 0 ? "false" : "true");
      }
    }
  }
  else if ( b1.length == b2.length )
  {
    /* element-wise equality  */
    for ( i = 0; i < b1.length; ++i )
    {
      if ( ISNUM(b2.array[i]) )
      {
        b2.array[i] = strdup(strtod(b2.array[i],NULL) == strtod(b1.array[i],NULL) ? \
            "0" : "1");
      }
      else
      {
        b2.array[i] = strdup(strcmp(b2.array[i],b1.array[i]) == 0 ? "false" : "true");
      }
    }
  }
  else
  {
    fprintf(stderr,"add: arrays are different lengths\n");
    /* exit( EXIT_FAILURE ); */
  }
  free_array(&b1);
  cat(a,&b2);
}

void fcn_greaterthan(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0}, b2 = {NULL, 0};
  int i;
  b1 = arpn(&b1,ntok,tokens); /* first */
  b2 = arpn(&b2,ntok,tokens); /* second */
  if ( b1.length  == 0 && b2.length > 0 )
  {
    /* default equal to 0 */
    for ( i = 0; i < b2.length; ++i )
    {
      if ( ISNUM(b2.array[i]) )
      {
        b2.array[i] = strdup(strtod(b2.array[i],NULL) > 0 ? "1" : "0");
      }
      else
      {
        b2.array[i] = strdup(*b2.array[i] == '\0' ? "false" : "true");
      }
    }
  }
  else if ( b1.length == 1 && b2.length > 0 )
  {
    /* comp b1 to elements of b2 */
    for ( i = 0; i < b2.length; ++i )
    {
      if ( ISNUM(b2.array[i]) )
      {
        b2.array[i] = strdup(strtod(b2.array[i],NULL) > strtod(b1.array[0],NULL) ? \
            "1" : "0");
      }
      else
      {
        b2.array[i] = strdup(strcmp(b2.array[i],b1.array[0]) > 0 ? "true" : "false");
      }
    }
  }
  else if ( b1.length == b2.length )
  {
    /* element-wise equality  */
    for ( i = 0; i < b1.length; ++i )
    {
      if ( ISNUM(b2.array[i]) )
      {
        b2.array[i] = strdup(strtod(b2.array[i],NULL) > strtod(b1.array[i],NULL) ? \
            "1" : "0");
      }
      else
      {
        b2.array[i] = strdup(strcmp(b2.array[i],b1.array[i]) > 0 ? "true" : "false");
      }
    }
  }
  else
  {
    fprintf(stderr,"add: arrays are different lengths\n");
    /* exit( EXIT_FAILURE ); */
  }
  free_array(&b1);
  cat(a,&b2);
}

void fcn_lessthan(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0}, b2 = {NULL, 0};
  int i;
  b1 = arpn(&b1,ntok,tokens); /* first */
  b2 = arpn(&b2,ntok,tokens); /* second */
  if ( b1.length  == 0 && b2.length > 0 )
  {
    /* default equal to 0 */
    for ( i = 0; i < b2.length; ++i )
    {
      if ( ISNUM(b2.array[i]) )
      {
        b2.array[i] = strdup(strtod(b2.array[i],NULL) < 0 ? "1" : "0");
      }
      else
      {
        b2.array[i] = strdup(*b2.array[i] == '\0' ? "true" : "false");
      }
    }
  }
  else if ( b1.length == 1 && b2.length > 0 )
  {
    /* comp b1 to elements of b2 */
    for ( i = 0; i < b2.length; ++i )
    {
      if ( ISNUM(b2.array[i]) )
      {
        b2.array[i] = strdup(strtod(b2.array[i],NULL) < strtod(b1.array[0],NULL) ? \
            "1" : "0");
      }
      else
      {
        b2.array[i] = strdup(strcmp(b2.array[i],b1.array[0]) < 0 ? "true" : "false");
      }
    }
  }
  else if ( b1.length == b2.length )
  {
    /* element-wise equality  */
    for ( i = 0; i < b1.length; ++i )
    {
      if ( ISNUM(b2.array[i]) )
      {
        b2.array[i] = strdup(strtod(b2.array[i],NULL) < strtod(b1.array[i],NULL) ? \
            "1" : "0");
      }
      else
      {
        b2.array[i] = strdup(strcmp(b2.array[i],b1.array[i]) < 0 ? "true" : "false");
      }
    }
  }
  else
  {
    fprintf(stderr,"add: arrays are different lengths\n");
    /* exit( EXIT_FAILURE ); */
  }
  free_array(&b1);
  cat(a,&b2);
}

void fcn_find(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0}, b2 = {NULL, 0};
  int i, j;
  b1 = arpn(&b1,ntok,tokens); /* if > 0 */
  b2 = arpn(&b2,ntok,tokens); /* search in */
  if ( b1.length  == 0 && b2.length > 0 )
  {
    /* nonzero elements of b2  */ 
    for ( i = 0, j = 0; i < b2.length; ++i )
    {
      if ( ISNUM(b2.array[i]) )
      {
        if ( strtod(b2.array[i],NULL) != 0 )
        {
          b2.array[j] = strdup(b2.array[i]);
          ++j;
        }
      }
      else
      {
        if ( *(b2.array[i]) != '\0' )
        {
          b2.array[j] = strdup(b2.array[i]);
          ++j;
        }
      }
    }
    b2.length = j;
    b2.array = realloc(b2.array, b2.length*sizeof(char*));
  }
  else if ( b1.length == b2.length ) 
  {
    /* find b2 such that b1 != 0 */
    for ( i = 0, j = 0; i < b1.length; ++i )
    {
      if ( ISNUM(b1.array[i]) )
      {
        if ( strtod(b1.array[i],NULL) != 0 )
        {
         b2.array[j] = strdup(b2.array[i]);
         ++j;
        }
      }
      else
      {
        if ( strcmp(b1.array[i],"true") == 0 )
        {
         b2.array[j] = strdup(b2.array[i]);
         ++j;
        }
      }
    }
    b2.length = j;
    b2.array = realloc(b2.array, b2.length*sizeof(char*));
  }
  else
  {
    fprintf(stderr,"add: arrays are different lengths\n");
    /* exit( EXIT_FAILURE ); */
  }
  free_array(&b1);
  cat(a,&b2);
}

void fcn_nth(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0}, b2 = {NULL,0}, b3 = {NULL,0}; 
  int i = 0, j = 0;
  b1 = arpn(&b1,ntok,tokens); /* array of elements to extract */
  b2 = arpn(&b2,ntok,tokens); /* array to extract nth element */
  rev(&b1);
  rev(&b2);
  for ( i = 0; i < b1.length; ++i )
  {
    j = strtol(b1.array[i],NULL,10);
    if ( j < b2.length )
    {
      append(&b3,b2.array[j]);  
    }
  }
  free_array(&b1);
  free_array(&b2);
  rev(&b3);
  cat(a,&b3);
}

void fcn_define(str_array *a, int *ntok, char *tokens[])
/* replace an element by a list */
{
  str_array b1 = {NULL, 0}, b2 = {NULL, 0}, b3 = {NULL, 0}, b4 = {NULL, 0};
  int i;
  b1 = arpn(&b1,ntok,tokens); /* search b1.length == 1 */
  b2 = arpn(&b2,ntok,tokens); /* replace b2.length >= 0 don't evaluate */
  b3 = arpn(&b3,ntok,tokens); /* in */
  if ( b1.length != 1 )
  {
    fprintf(stderr,"define: search term not of length 1: "
        "(%d)\n", b1.length);
    /* exit( EXIT_FAILURE ); */
  }
  i = 0;
  while ( i < b3.length )
  {  
    if (strcmp(b1.array[0],b3.array[i]) == 0)
    {
      delete(&b3, i);
      insert(&b3, &b2, i);
      i += b2.length - 1;
    }
    i++;
  }
  free_array(&b1);
  free_array(&b2);
  rev(&b3); 
  printf("b3 = ");  
  printa(b3);  
  b4 = arpn(&b4,&(b3.length),b3.array);
  /* rev(&b4); */
  free_array(&b3);
  cat(a,&b4);
}

void fcn_let(str_array *a, int *ntok, char *tokens[])
{
  str_array b1 = {NULL, 0}, b2 = {NULL, 0};
  char c;
  int bntok = 0;
  b1 = arpn(&b1,ntok,tokens); /* key: must be a char A-Z */
  b2 = arpn(&b2,ntok,tokens); /* val: array s converted to string with join_elements */ 
  (void)a; /* a is not used in the let function */
#ifdef MYDEBUG
  printf("let: b1 = %s\n",b1.array[0]);
#endif
  /* 'A' == 65, 'Z' == 90 */
  c = *(b1.array[0]);
  if ( ( c > 64 ) && ( c < 91 ) )
  {
      rev(&b2);
      join_elements(char_register[c - 65].val, 128, b2, &bntok, " ");
#ifdef MYDEBUG
      printf("let: %c = %s\n",char_register[c - 65].key, char_register[c - 65].val);
#endif
      rev(&b2);
  }
  free_array(&b1);
  free_array(&b2);
  /* cat(a,&b2); */
}

void fcn_duplicate(str_array *a, int *ntok, char *tokens[])
{
  int here = *ntok;
  str_array b1 = {NULL, 0};
  b1 = arpn(&b1,ntok,tokens); /* get next array */
  *ntok = here;
  cat(a,&b1);
}

void fcn_range(str_array *a, int *ntok, char *tokens[])
{
  str_array r = {NULL, 0};
  double x = 0.0; double x1; 
  double h = 0.0;
  char s[20];
  str_array tmp = {NULL, 0};
  r = arpn(&r,ntok,tokens); /* range, must be length 2 or 3 */
  x1 = strtod(r.array[0],NULL);
  switch(r.length)
  {
    case 2: 
      h = 1.0;
      x = strtod(r.array[1],NULL);
      break;
    case 3:
      h = strtod(r.array[1],NULL);
      x = strtod(r.array[2],NULL);
      break;
    default:
      h = 1.0;
      x = 0.0;
  }
  /* range */
  if ( h <= 0.0 )
  {
    fprintf(stderr,"range: step h not positive\n");
  }
  else
  {  
    while ( x < x1)
    {
      sprintf(s,"%g",x);
      append(&tmp, s);
      x += h;
    }
  }
  rev(&tmp);
  free_array(&r);
  cat(a,&tmp);
}

void fcn_date(str_array *a, int *ntok, char *tokens[])
{
  struct tm tm0 = {0,0,0,1,0,0,0,0,0,0,""}, 
            tm1 = {0,0,0,1,0,0,0,0,0,0,""};
  time_t tt0, tt1;
  char s[32];
  str_array dat = {NULL, 0};
  dat = arpn(&dat,ntok,tokens); /* date in format ddmmyyyy, must be length 2 */

  s[2] = 0;
  s[5] = 0;
  s[10]= 0;
  strncpy(s,dat.array[0],2);              /* day */
  strncpy(s + 3,dat.array[0] + 2,2);      /* month */
  strncpy(s + 6,dat.array[0] + 4,4);      /* year */
  /* s = "dd\0mm\0yyyy\0" */
  tm0.tm_mday = (int)strtol(s,NULL,10);
  tm0.tm_mon  = (int)strtol(s + 3,NULL,10) - 1;
  tm0.tm_year = (int)strtol(s + 6,NULL,10) - 1900;

  strncpy(s,dat.array[1],2);              /* day */
  strncpy(s + 3,dat.array[1] + 2,2);      /* month */
  strncpy(s + 6,dat.array[1] + 4,4);      /* year */
  tm1.tm_mday = (int)strtol(s,NULL,10);
  tm1.tm_mon  = (int)strtol(s + 3,NULL,10) - 1;
  tm1.tm_year = (int)strtol(s + 6,NULL,10) - 1900;
  tt0 = mktime(&tm0);
  tt1 = mktime(&tm1);
  sprintf(s,"%g",difftime(tt1,tt0));
  free_array(&dat);
  append(a, s);
}

str_array arpn(str_array *a, int *ntok, char *tokens[])
{
    char c;

    /* printf("literal = %d\n",literal); */
    if ( *ntok == 0 )
    {
      return *a;
    }
    (*ntok)--;

    c = *tokens[*ntok];
    /* char 39 == single quote ' */ 
    /* if token starts with single quote and not in a literal block, discard token
       and start literal block */
    if ( c == 39 && literal == 0 ) /* start litteral block */
    {
      literal++; 
      if ( literal == 1 )
      {
        *a = arpn(a,ntok,tokens);
        return *a;
      } 
    }
    /* if token starts with single quote and is in a literal block, discard token 
       and end literal block */
    if ( c == 39 && literal > 0 ) /* end litteral block */
    {
      literal--; 
      if ( literal == 0 )
      {
        *a = arpn(a,ntok,tokens);
        return *a;
      }
    }

    if ( literal == 0 ) /* evaluate list */
    {
      if ( ! ( isdigit((char) *tokens[*ntok]) || *tokens[*ntok] == '-' ) )
      {
        int i;
        /* end of array -- return it */
        if ( c == ',' || c == '(' )
        {
          return *a;
        }

        /* try to find command name */
        for ( i = 0;  i < ARPN_NBR_FCNS; ++i)
        {
          if( strncmp(fcns[i].name, tokens[*ntok], 7) == 0 )
          {
            (*(fcns[i].func))(a, ntok, tokens);
            return *a;
          }
        }
      }
    }

    /* just append to current array a */
    append(a,tokens[*ntok]);
    *a = arpn(a,ntok,tokens);
    return *a;
}


str_array repl(char *cmd)
{
  str_array a = {NULL, 0};
  char **ap, **tokens;
  int ntok = ARPN_MAX_NBR_TOKEN;
  int state = 0; /* 0: outside "", 1: inside "" */
  char *line = NULL, 
       *cmd2, 
       *c, 
       *c2;

  if ( strlen(cmd) == 0 )
  {
    return a;
  }

  cmd[strcspn(cmd,"\n")] = '\0';

  /* insert spaces around non-space delimiters ( ) , ; */
  cmd2 = malloc(((3*strlen(cmd))+1)*sizeof(char));
  for (c = cmd, c2 = cmd2; *c != '\0'; c++, c2++ )
  {
    *c2 = *c;
    if ( *c == '"' ) /* toggle state */
    {
      state = (1-state);
    }
    if ( state == 0 )
    {
      if ( *(c+1) == '(' || *(c+1) == ')' || *(c+1) == 39 || 
           *(c+1) == ',' || *(c+1) == ';' )
      {
        if ( *(c) != ' ' )
        {
          *(++c2)  = ' ';
        }
      }
      if ( *c == '(' || *c == ')' || *c == 39 ||
           *c == ',' || *c == ';' )
      {
        if ( *(c+1) != ' ' )
        {
          *(++c2) = ' ';
        }
      }
    }
  }
  *(c2) = '\0';

#ifdef MYDEBUG
  printf("debug<- %s\n", cmd2); 
#endif

  /* extract tokens, including non space delimiters */
  line = cmd2;
  ntok = ARPN_MAX_NBR_TOKEN; 
  tokens = malloc(ntok*sizeof(char*));
  for (ap = tokens; (*ap = strsep(&line, " ")) != NULL;)
  {
    if ( line != NULL && *line == '"' )
    {
      *(++ap) = line; 
      line += 2 + strcspn(line+1,"\"");       
      if ( *line != '\0' )
      {
        *(line++) = '\0';
      }
    }

    /* printf("token: \"%s\"\n",*ap); */
    if (**ap != '\0')
    {
      if (++ap >= &tokens[ntok])
      {
        fprintf(stderr,"number of tokens exceed the max capacity %d\n", ntok);
        /* exit ( EXIT_FAILURE ); */
      }
    }
  }
  ntok = ap - tokens;
  tokens = realloc(tokens,ntok*sizeof(char*));

  a.array = NULL;
  a.length = 0;
  literal = 0;
  a = arpn (&a, &ntok, tokens);
  rev(&a);

#ifdef MYDEBUG
  printf("debug-> ");
  printa(a);
#endif
  
  free(tokens);
  free(cmd2);

  return a;
}

