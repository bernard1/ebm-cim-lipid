/* lrexp.h
 * computes the coupling term 
 *
 *   y_i = sum_{j=1:N} w_ij * f(x_j - x_i)
 *
 * in the most efficient way
 */

#ifndef _LREXP_H_
#define _LREXP_H_

#ifndef _ODEXP_H_
typedef double (*coupling_function)(double);
#endif

int lrexpars(const double *x, const int N, double *meanx, double *range);
/*  lrexpars: compute lr expansion parameters
 *  input: state vector x, size N
 *  outpout: meanx mean of x
 *           range array [min,max] of x
 */

int lrexprank(coupling_function f, const int N, int *p, const double range);
/*  lrexprank: compute tolerance-based lr expansion rank
 *  input:  f     coupling function 
 *          N     vector size
 *          range range of state vector
 *  output: p     expansion rank
 */

int lrexp(coupling_function f, const double *x, double *y, const int N, const int p, const double meanx, const double range);
/* lrexp: computes polynomial expansion of the coupling function
 * input:   f     coupling function
 *          x     state vector
 *          N     size of vector
 *          p     expansion rank
 *          meanx mean of x
 *          range range of x
 * output:  y     approximation y[i] = sum_j f(x[j] - x[i])
 */

int lrexpw(coupling_function f, const double *U, const double *V, const int r, const double *x, double *y, const int N, const int p, const double meanx, const double range);
/*  lrexpw: compute a low-rank polynomial expansion of 
 *
 *             y[i] = sum_j W_ij f(x[j] - x[i])
 *
 *          for i,j = 1, ...,N
 *
 *          The NxN matrix W is of low rank r and is factorized 
 *
 *          W = U*V
 *
 *  input:  U     Nxr matrix
 *          V     rxN matrix
 *          f     coupling function
 *          x     state vector
 *          N     size of vector
 *          p     expansion rank
 *          meanx mean of x
 *          range range of x
 *
 *          W is computed as
 *
 *             W_ij = sum_m=1^r U_im V_mj
 * 
 * The algorithm is based on Chebychev approximation of the
 * function f(u) on u in [-range, range]
 * The algorithm runs in O(N*P^2), and is faster than the
 * direct method if N > P^2. Numerical tests show P up to 
 * 30. Advantageous if N > 2000
 *
 * the coupling term is return in the array y.
 *
 */


int lrexpwp(const int iu, const int iv, const int r, coupling_function f, const double *x, double *y, const int N, const int p, const double meanx, const double range);
/*  lrexpwp: low rank expansion for population-based system
 *           compute a polynomial approximation of the coupling term
 *
 *               y[i] = 1/N*sum_j W_ij f(x[j] - x[i])
 *
 *          for i,j = 1, ...,N
 *
 *          The NxN matrix W is of low rank r and is factorized 
 *
 *          W = U*V
 *
 *  input:  ui    index of a parametric expression for the row of U
 *          vi    index of a parametric expression for the columns of V
 *          f     coupling function
 *          x     state vector
 *          N     size of vector
 *          p     expansion rank
 *          meanx mean of x
 *          range range of x
 *
 *          W is computed as
 *
 *             W_ij = sum_m=1^r U_im V_mj
 * 
 *
 * The algorithm is based on Chebychev approximation of the
 * function f(u) on u in [-range, range]
 * The algorithm runs in O(N*P^2), and is faster than the
 * direct method if N > P^2. Numerical tests show P up to 
 * 30. Advantageous if N > 2000
 *
 * the coupling term is return in the array y.
 *
 */

/* USER FUNCTIONS */
int lrkern(coupling_function f, const double *x, double *y, const int N);
/*  lrkern: polynomial low-rank expansion of 
 *
 *             y[i] = sum_j f(x[j] - x[i])
 *
 *          for i,j = 1, ...,N
 *  
 *  input:  f     coupling function
 *          x     state vector
 *          N     size of vector
 *  output: y     approximation
 */

int lrwkern(const double *U, const double *V, const int r, coupling_function f, const double *x, double *y, const int N);
/*  lrwkern: compute a low-rank polynomial expansion of 
 *
 *             y[i] = sum_j W_ij f(x[j] - x[i])
 *
 *          for i,j = 1, ...,N
 *
 *          The NxN matrix W is of low rank r and is factorized 
 *
 *          W = U*V
 *
 *  input:  U     Nxr matrix
 *          V     rxN matrix
 *          f     coupling function
 *          x     state vector
 *          N     size of vector
 *          p     expansion rank
 *          meanx mean of x
 *          range range of x
 *
 *          W is computed as
 *
 *             W_ij = sum_m=1^r U_im V_mj
 *  output: y     approximation
 *
 */

int lrwpkern(const int iu, const int iv, const int r, coupling_function f, const double *x, double *y, const int N);
/*  lrwpkern: low rank expansion for population-based system
 *           compute a polynomial approximation of the coupling term
 *
 *               y[i] = 1/N*sum_j W_ij f(x[j] - x[i])
 *
 *          for i,j = 1, ...,N
 *
 *          The NxN matrix W is of low rank r and is factorized 
 *
 *          W = U*V
 *
 *  input:  ui    index of a parametric expression for the row of U
 *          vi    index of a parametric expression for the columns of V
 *          f     coupling function
 *          x     state vector
 *          N     size of vector
 *          p     expansion rank
 *          meanx mean of x
 *          range range of x
 *
 *          W is computed as
 *
 *             W_ij = sum_m=1^r U_im V_mj
 *
 *  output: y     approximation
 *
 */

#endif
