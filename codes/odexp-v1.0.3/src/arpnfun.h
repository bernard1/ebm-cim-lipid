#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

#include "odexp.h"

#define ARPN_EXPRESSION_LENGTH 5096
#define ARPN_MAX_NBR_TOKEN 1024
#define ARPN_NBR_CHAR_REGISTER 26 
#define ARPN_NBR_FCNS 24

typedef struct str_array
{
    char **array;
    int length;
} str_array;

typedef void (*array_func_t)(str_array*, int *, char *tokens[]);

typedef struct array_function {
  char *name;
  array_func_t func;
} func;

typedef struct key_value {
  char key;
  char val[128];
} keyval;

/* entry point */ 
str_array repl(char *cmd);                                  

/* manipulate arrays of tokens */
char*   getnextt(const str_array a, int *n);
int     getnargs(const str_array, int n);
void    joinargs(char chptr[], size_t cap, const str_array a, int *n, const char *sep);
void    join_elements(char chptr[], size_t cap, const str_array a, int *n, const char *sep);

/* helper functions */
void expand_variable(char *val, const char *tok);
void printa(str_array a);
void rev(str_array *a);
void cpy(const str_array *source, str_array *dest);
void append(str_array *a, char *x);
void cat(str_array *a, str_array *b);
void insert(str_array *a, const str_array *ins, int loc);
void split(str_array *a, str_array *b, int loc);
void asnumerical(const str_array * restrict a, double **ax);
void aschar(str_array *a, double *ax);
void free_array(str_array *a);
str_array arpn(str_array *a, int *ntok, char *tokens[]);

/* rpn functions  */
void fcn_add(str_array *a, int *ntok, char *tokens[]);            /* add */
void fcn_mult(str_array *a, int *ntok, char *tokens[]);           /* mult */
void fcn_power(str_array *a, int *ntok, char *tokens[]);          /* power */
void fcn_define(str_array *a, int *ntok, char *tokens[]);         /* def */
void fcn_let(str_array *a, int *ntok, char *tokens[]);            /* let */
void fcn_equal(str_array *a, int *ntok, char *tokens[]);          /* equal */
void fcn_neq(str_array *a, int *ntok, char *tokens[]);            /* notequal */
void fcn_greaterthan(str_array *a, int *ntok, char *tokens[]);    /* > */
void fcn_lessthan(str_array *a, int *ntok, char *tokens[]);       /* < */
void fcn_find(str_array *a, int *ntok, char *tokens[]);           /* find */
void fcn_nth(str_array *a, int *ntok, char *tokens[]);            /* nth */
void fcn_replicate(str_array *a, int *ntok, char *tokens[]);      /* rep */
void fcn_range(str_array *a, int *ntok, char *tokens[]);          /* range */
void fcn_reverse(str_array *a, int *ntok, char *tokens[]);        /* reverse */
void fcn_interleave(str_array *a, int *ntok, char *tokens[]);     /* interleave */
void fcn_concatenate(str_array *a, int *ntok, char *tokens[]);    /* cat */ 
void fcn_join(str_array *a, int *ntok, char *tokens[]);           /* join */
void fcn_quote(str_array *a, int *ntok, char *tokens[]);          /* quote */
void fcn_eval(str_array *a, int *ntok, char *tokens[]);           /* eval */
void fcn_ifelse(str_array *a, int *ntok, char *tokens[]);         /* ifelse */
void fcn_length(str_array *a, int *ntok, char *tokens[]);         /* len */
void fcn_duplicate(str_array *a, int *ntok, char *tokens[]);      /* dup */
void fcn_reshape(str_array *a, int *ntok, char *tokens[]);        /* reshape */
void fcn_date(str_array *a, int *ntok, char *tokens[]);           /* timediff */
