/* file main.c */

/* includes */
#include <gsl/gsl_multiroots.h>
#include <gsl/gsl_eigen.h> 
#include <readline/readline.h>
#include <readline/history.h>                             
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <sys/ioctl.h>

#include "main.h"
#include "odexpConfig.h"
#include "output_manager.h"

#define EXIT_IF_NO_FILE      1
#define DONT_EXIT_IF_NO_FILE 0

/* variable for holding the command line string */
char *rawcmdline = (char *)NULL;

/* log file */
FILE *logfr   = (FILE *)NULL;
FILE *dblogfr = (FILE *)NULL;
FILE *GPLOTP  = (FILE *)NULL;
int   GPIN; /* file descriptor for fifo */

/* world */
world *SIM = (world *)NULL;

/* warning/error simulation messages */
msg_buff MSG_BUFF = {0, 0, {""}};


static char dacme[6] = "DACME";

extern int literal;

/* int ode_system_size; */

/* =================================================================
   Main Function 
   ================================================================ */
int odexp( oderhs pop_ode_rhs, oderhs single_rhs, odeic pop_ode_ic, odeic single_ic, rootrhs root_rhs, const char *odexp_filename )
{

  /* variable declaration */
  time_t now = time(NULL);

  /* files */
  char       new_par_filename[MAXFILENAMELENGTH];
  char       data_fn[NAMELENGTH]; /* dataset filename */

  /* commands and options */
  char    par_details[32];
  char    list_msg[EXPRLENGTH];
  char    plot_str_arg[EXPRLENGTH];
  char    plot_animate_string[EXPRLENGTH];
  char    op,
          op2;
  int     rep_command = 1;
  double  nvalue;
  char    svalue[EXPRLENGTH],
          svalue2[EXPRLENGTH];
  int     np,
          padding,
          nbr_read,
          nbr_hold = 0;
  struct plotindex pi = { 1, 2, 3, -1, 0, 1 };
          
  int  p = 0; /* active parameter index */
  int  prompt_index = 0;
  char plotkeys[MAX_PLOT_KEY][EXPRLENGTH];

  /* iterators */
  int  i,j;

  /* status */
  int     err;

  /* modes */
  int     sim_status = SS_NULL;

  /* 
   * plot mode:
   * 0: PM_UNDEFINED undefined
   * 1: PM_NORMAL normal update plot with new parameters/option
   * 2: PM_FUN  like normal plot but with functions of variables 
   * 3: PM_ANIMATE  like normal update plot with new parameters/option but animate solution
   * 4: PM_CONTINUATION continuation plot continuation branch 
   * 5: PM_PARTICLES particles in phase space
   * 6: PM_CURVES add curves 
   */
  enum plotmode plot_mode       = PM_UNDEFINED; 

  /* sizes */
  int ode_system_size; /* number of dynamical variables */
  int total_nbr_x;  /* number of dependent and auxiliary variables */
  int nbr_col;     /* number of columns written in particle files id.dat */

  /* tspan parameters */
  const char      ts_string[] = "TIMESPAN"; 
  const int       ts_len = 5;
  double_array    tspan;
  
  /* nve */
  nve pex;     /* parametric expressions */
  nve func;    /* user-defined functions */
  nve mu;      /* parameters */
  nve ics;     /* initial conditions */
  nve fcn;     /* auxiliary functions */
  nve eqn;     /* dynamical equations */
  nve psi;     /* pop coupling terms */
  nve mfd;     /* pop mean fields/stats */
  nve dxv;     /* list of all Dynamical  + auXiliary Variables */
  nve cst;     /* constant arrays */
  nve dfl;     /* data files */
  nve dsc;     /* short description */
  nve xcd;     /* extra gnuplot commands */

  nve birth;   /* population birth rates */
  nve repli;   /* particle replication rates */
  nve death;   /* particle death rates */

  /* range double_array */
  double_array    range_array = {(double *)NULL, 0};

  /* particle */
  /* par *pars = (par *)NULL; */

  /* temp double_array */
  /* double_array    arr = {NULL, 0}; */

  /* steady states */
  steady_state    *stst = NULL;
  int             nbr_stst = 0;
  /* end variable declaration */

  /* rpn command */
  str_array a = {NULL, 0};
  int ntok;
  char *tok;


  /* sleep */
  struct timespec rqtp = {0.0,0};

  /* load options */
  load_options(PAR_FILENAME, EXIT_IF_NO_FILE); 


  /* set xterm title */
  PRINTV("\033]0;odexp\007");


  if ( ( logfr = fopen(LOG_FILENAME, "a") ) == NULL ) /* create a log file or append an existing log file */
  {
    PRINTERR("  Error: could not open log file '%s', exiting...",LOG_FILENAME);
    exit ( EXIT_FAILURE );
  }
  if ( ( dblogfr = fopen(DB_LOG_FILENAME, "w") ) == NULL ) /* create a debug log file or append an existing log file */
  {
    PRINTERR("error: could not open log file '%s', exiting...",DB_LOG_FILENAME);
    exit ( EXIT_FAILURE );
  }
  PRINTLOG("#--\nLog for model %s", odexp_filename); 
  DBLOGPRINT("Log for model %s", odexp_filename); 
  PRINTLOG("%s", ctime( &now )); 


#ifdef MYDEBUG
  printf("\nRunning in debug mode\n");
#endif

  /* begin */
  PRINTV("\nodexp file: %s%s%s\n",T_VAL,odexp_filename,T_NOR);

  /* get short description */
  PRINTV("\n%-25s" HLINE "\n", "model description");
  setup_nve(EQ_FILENAME, "##", 2, 'l', 0, &dsc);
  for ( i = 0; i<dsc.nbr_el; i++ )
  {
    PRINTV("%s\n",dsc.comment[i]);  
  }
  DBLOGPRINT("%d description",dsc.nbr_el);

  /* get tspan */
  load_double_array(PAR_FILENAME, &tspan, ts_string, ts_len, EXIT_IF_NO_FILE); 
  if (tspan.length == 0)
  {
    PRINTWARNING("\n  Warning: time span not found. Time span will be set to default [0,1]\n"
        "  (One line in file %s should be of the form\n"
        "  TIMESPAN 0 100)\n", odexp_filename);
    DBLOGPRINT("Warning: time span not found.");
    tspan.array = malloc(2*sizeof(double));
    tspan.length = 2;
    tspan.array[0] = 0.0;
    tspan.array[1] = 1.0;
  }
  else
  {
    /* printf("  %d time points, of which %d stopping points\n", tspan.length, tspan.length - 2); */
    DBLOGPRINT("%d time points, of which %d stopping points", tspan.length, tspan.length - 2);
  }

  /* extra commands */
  setup_nve(EQ_FILENAME, "CMD", 3, 'l', 0, &xcd);
  DBLOGPRINT("%d extra commands",xcd.nbr_el);

  /* get constant arrays */
  setup_nve(EQ_FILENAME, "CONST", 5, 'l', 0, &cst);
  DBLOGPRINT("%d constants",cst.nbr_el);

  /* get data files */
  setup_nve(EQ_FILENAME, "FI", 2, 's', 1, &dfl);
  DBLOGPRINT("%d data files",dfl.nbr_el);

  /* get user-defined functions */
  setup_nve(EQ_FILENAME, "FUN", 2, 's', 1, &func);
  DBLOGPRINT("%d user-defined function",func.nbr_el);

  /* get parameters */
  setup_nve(PAR_FILENAME, "PAR", 3, 'n', 0, &mu);
  if (mu.nbr_el == 0) /* then create a not_a_parameter parameter */
  {
    /* printf("  no parameter\n"); */
    mu.value = realloc(mu.value,sizeof(double));
    mu.name = realloc(mu.name,sizeof(char*));
    mu.name[0] = malloc(NAMELENGTH*sizeof(char));
    mu.expression = realloc(mu.expression,sizeof(char*));
    mu.expression[0] = malloc(EXPRLENGTH*sizeof(char));
    mu.attribute = realloc(mu.attribute,sizeof(char*));
    mu.attribute[0] = malloc(EXPRLENGTH*sizeof(char));
    mu.comment = realloc(mu.comment,sizeof(char*));
    mu.comment[0] = malloc(EXPRLENGTH*sizeof(char));
    mu.nbr_el = 1;
    mu.nbr_expr = 1;
    strncpy(mu.name[0],"--",NAMELENGTH);
    strncpy(mu.attribute[0],"not a parameter",NAMELENGTH);
    mu.value[0] = NAN;
    *mu.max_name_length = 15; 
  } 
  DBLOGPRINT("%d parameters",mu.nbr_el);

  /* get parametric expressions */
  setup_nve(EQ_FILENAME, "EXPR", 4, 's', 1, &pex);
  DBLOGPRINT("%d parametric expressions",pex.nbr_el);

  /* get initial conditions */
  setup_nve(PAR_FILENAME, "INIT", 4, 's', 1, &ics);
  if (ics.nbr_el == 0)
  {
    PRINTERR("\n  Warning: Initial conditions not found.\n"
        "  File %s should contain initial condition for each dynamical variables "
        "with lines of the form\n"
        "  INIT VAR VALUE\n", odexp_filename);
    DBLOGPRINT("Error: Initial conditions not found.");
    /* exit ( EXIT_FAILURE ); */
  } 
  DBLOGPRINT("%d variables",ics.nbr_el);


  /* get nonlinear functions */
  setup_nve(EQ_FILENAME, "AUX", 3, 's', 1, &fcn);
  DBLOGPRINT("%d auxiliary variables",fcn.nbr_el);

  /* get equations */
  setup_nve(EQ_FILENAME, "d", 1, 's', 0, &eqn);
  if (eqn.nbr_el == 0)
  {
    PRINTERR("\n  Warning: Equations not found."
        "  File %s should contain equations for each dynamical variables "
        "with lines of the form\n"
        "  dX/dt = RHS\n", odexp_filename);
    DBLOGPRINT("Error: Equations not found.");
    /* exit ( EXIT_FAILURE ); */
  } 
  DBLOGPRINT("%d equations",eqn.nbr_el);

  /* define psi */
  setup_nve(EQ_FILENAME, "%C", 2, 's', 1, &psi);
  DBLOGPRINT("%d population couplings",psi.nbr_el);

  /* define mfd */
  setup_nve(EQ_FILENAME, "%M", 2, 's', 1, &mfd);
  DBLOGPRINT("%d population mean fields",mfd.nbr_el);

  /* define birth */
  setup_nve(EQ_FILENAME, "%BIRTH", 6, 's', 0, &birth);
  DBLOGPRINT("%d population birth rates",birth.nbr_el);

  /* define repli */
  setup_nve(EQ_FILENAME, "%REPLI", 6, 's', 0, &repli);
  DBLOGPRINT("%d population replication rates",repli.nbr_el);

  /* define death */
  setup_nve(EQ_FILENAME, "%DEATH", 6, 's', 0, &death);
  DBLOGPRINT("%d population death rates",repli.nbr_el);

  /* define dxv */
  ode_system_size = ics.nbr_el;
  /* total number of dependent variables: y, aux, psi, mfd, pex */
  total_nbr_x = ics.nbr_el + fcn.nbr_el + psi.nbr_el + mfd.nbr_el + pex.nbr_el; 
  nbr_col = 1 + total_nbr_x; 
  dxv.nbr_expr = ics.nbr_expr + fcn.nbr_expr + psi.nbr_expr + mfd.nbr_expr + pex.nbr_expr;
  dxv.nbr_el = total_nbr_x;
  alloc_namevalexp(&dxv);
  *dxv.max_name_length = max(*pex.max_name_length,max(*mfd.max_name_length,max(*psi.max_name_length, max(*ics.max_name_length, *fcn.max_name_length))));
  for (i = 0; i < ode_system_size; i++)
  {
    strcpy(dxv.name[i],ics.name[i]);
    strcpy(dxv.expression[i],ics.expression[i]);
    strcpy(dxv.attribute[i],ics.attribute[i]);
  }
  j = ode_system_size;
  for (i = 0; i < fcn.nbr_el; i++)
  {
    strcpy(dxv.name[j+i],fcn.name[i]);
    strcpy(dxv.expression[j+i],fcn.expression[i]);
    strcpy(dxv.attribute[j+i],fcn.attribute[i]);
  }
  j += fcn.nbr_el;
  for (i = 0; i < psi.nbr_el; i++)
  {
    strcpy(dxv.name[j+i],psi.name[i]);
    strcpy(dxv.expression[j+i],psi.expression[i]);
    strcpy(dxv.attribute[j+i],psi.attribute[i]);
  }
  j += psi.nbr_el;
  for (i = 0; i < mfd.nbr_el; i++)
  {
    strcpy(dxv.name[j+i],mfd.name[i]);
    strcpy(dxv.expression[j+i],mfd.expression[i]);
    strcpy(dxv.attribute[j+i],mfd.attribute[i]);
  }
  j += mfd.nbr_el;
  for (i = 0; i < pex.nbr_el; i++)
  {
    strcpy(dxv.name[j+i],pex.name[i]);
    strcpy(dxv.expression[j+i],pex.expression[i]);
    strcpy(dxv.attribute[j+i],pex.attribute[i]);
  }


  /* manage options */
  update_plot_index(&pi, dxv); /* set plot index from options, if present */
  update_plot_options(pi,dxv); /* set plot options based to reflect plot index */
  update_act_par_index(&p, mu);
  update_act_par_options(p, mu);
  /* set wintitle to odefilename if not set */
  if ( strlen(get_str("wintitle")) == 0 )
  {
    set_str("wintitle",odexp_filename);
  }
  /* printf_options(""); */
  /* printf("  options loaded (type 'lo' to see all options)\n"); */
  DBLOGPRINT("Options loaded");

  /* initialize world SIM       */
  SIM = malloc(sizeof(world));
  init_world( SIM, &pex, &func, &mu, &ics, &fcn, &eqn, &psi, &mfd, &dxv, &cst, &dfl, pop_ode_rhs);

  /* seed random number generator */
  srand( (unsigned long)get_int("seed") );
  PRINTLOG("Rand seed: %d",get_int("seed"));
  /* test rng */
  /* printf("  RAND_MAX %s%d%s\n\n",T_VAL,RAND_MAX,T_NOR); */
  PRINTLOG("RAND_MAX %d",RAND_MAX);

  PRINTV("  logfiles: " LOG_FILENAME " and " DB_LOG_FILENAME "\n\n");

  /* readline */
  if (strcmp("EditLine wrapper",rl_library_version) == 0)
  {
    PRINTWARNING("warning: You are using the EditLine wrapper of the readline library.\n");    
    PRINTWARNING("         inputrc will not work and you will not be able to use the keyboard shortcuts\n\n");
    DBLOGPRINT("warning: You are using the EditLine wrapper of the readline library.");    
  }
  else if ( rl_read_init_file (ODEXPDIR ".inputrc") ) /* readline init file */
  {
    PRINTWARNING("\n  warning: inputrc file for readline not found\n");
    DBLOGPRINT("warning: inputrc file for readline not found");
  }
  initialize_readline();
  rl_attempted_completion_function = completion_list_completion;

  /* history - initialize session */
  using_history();
  if ( read_history(HISTORY_FILENAME) )
  {
    DBLOGPRINT("warning: history file " HISTORY_FILENAME " not found");
  }

  stifle_history( HISTORY_SIZE );

  /* first run after system init */
  PRINTLOG("System init done.");
  DBLOGPRINT("System init done.");

  /* if loudness == silent, skip command line and quit */
  if ( strncmp(get_str("loudness"),"silent",3) == 0 ) /* run silent mode, break out of while loop  */
  {
    set_int("progress",0);
    PRINTLOG("Silent mode, will exit after first simulation");
    sim_status = SS_ACTION_QUIT;
  }

  /* if loudness == quiet, print out normal information, skip command line and quit */ 
  if ( strncmp(get_str("loudness"),"quiet",3) == 0 ) /* run quiet mode, break out of while loop  */
  {
    printf("\n  %syou are in quiet mode (option loudness quiet),\n"
        "  I will now exit, but leave output files in place.%s\n\n", T_DET,T_NOR);    
    printf("  hexdump -e '%d \"%%5.2f \" 5 \"%%5d \" \"\\n\"' stats.dat\n", 1+mfd.nbr_el); 
    printf("  hexdump -e '2 \"%%d \" %d \"%%5.2f \" \"\\n\"' traj.dat\n\n", nbr_col); 
    printf("  hexdump -e '3 \"%%5.2f \" \"\\n\"' " QUICK_BUFFER "\n\n"); 
    PRINTLOG("Quiet mode, will exit after first simulation");
    sim_status = SS_ACTION_QUIT;
  }

  if ( get_int("runonstartup") == 1 )
  {    
    PRINTLOG("Running first simulation");
    odesolver(pop_ode_rhs, single_rhs, pop_ode_ic, single_ic, &tspan);
    PRINTLOG("First simulation done.");
    sim_status |= SS_MODIFIED_RUN;
  }


  if ( strncmp(get_str("loudness"),"loud",3) == 0 ) /* run loud, with gnuplot pipe  */
  {
    if ( mkfifo(GP_FIFONAME, 0600) ) 
    {
      if (errno != EEXIST) 
      {
        PRINTERR("%s", GP_FIFONAME);
        unlink(GP_FIFONAME);
        return 1;
      }
    }
    if ( ( GPLOTP = popen("gnuplot -persist >>" GP_FIFONAME " 2>&1","w") ) == NULL ) /* open gnuplot in 
                                                                                      persist mode with 
                                                                                      redirection of stderr > stdout */
    {
      PRINTERR("gnuplot failed to open");
      pclose(GPLOTP);
      return 1;
    }
    fprintf(GPLOTP,"set print \"%s\"\n", GP_FIFONAME); /* redirect output to GP_FIFONAME */  
    fflush(GPLOTP);
    if ( ( GPIN = open(GP_FIFONAME, O_RDONLY | O_NONBLOCK) ) == -1 )
    {
      PRINTERR("Could not open named pipe %s", GP_FIFONAME);
      close(GPIN);
      return 1;
    }
    DBLOGPRINT("popen gnuplot"); 
  
    /* GNUPLOT SETUP */
    gnuplot_config();
    /* END GNUPLOT SETUP */ 
  }

  PRINTLOG("Main loop");
  while(! ( sim_status & SS_ACTION_QUIT ))  /* MAIN LOOP */
  {

    /* first read any incoming messages from gnuplot and print them */
    while ( read_msg() ) /* try to read and print FIFO */
    {
      /* if already caught a message, try to catch more of the message */ 
    };

    printf("%s",T_NOR); /* reset normal terminal format */
    
    /* execute gplot before command */
    fprintf(GPLOTP,"%s\n", get_str("before"));
    fflush(GPLOTP);

    /* BEGIN get command line */

    free(rawcmdline);
    printf_status_bar( &tspan );
    rawcmdline = readline(ODEXP_PROMPT(prompt_index));
    printf("%s","\033[J"); /* clear to the end of screen */
    if ( strlen(rawcmdline) > 0 ) /* add the full raw, unprocessed cmdline to history if non empty and 
                                   * if it read from the command line */
    {
      prompt_index++;             /* prompt number--serves no real purpose */
      add_history (rawcmdline);
    }
    /* END get command line */

    PRINTLOG("> %s",rawcmdline);
    
    free_array(&a);

#ifdef MYDEBUG
    DBPRINT("use ':' to escape the repl");
#endif 
    if ( rawcmdline[strspn(rawcmdline," ")] == ':' )
    {
      a.length = 1;
      a.array  = malloc(sizeof(char*));
      a.array[0]  = malloc(strlen(rawcmdline+1)*sizeof(char));
      strncpy(a.array[0],rawcmdline,strlen(rawcmdline));
      a.array[0][strlen(a.array[0])-1] = '\0';
    }
    else
    {
      a = repl(rawcmdline);        /* parse command line 
                                  * 1. break command line into space-separated tokens
                                  * 2. parse each token RPN-style
                                  * 3. () delimits arrays
                                  * 4. "some strings" are treated as one token 
                                  * 5. 'literal string' is not parsed, but quotes are removed
                                  * 6. tokens are expanded with eval
                                  * 7. tokens are expanded during command interpretation
                                  */
    }

    ntok = 0;
    while ( ntok < a.length )
    {
      tok = getnextt(a, &ntok);
      switch(*tok)
			{
        case '^':  /* do not evaluate any expression */
          while ( (tok = getnextt(a, &ntok)) != NULL )   
          {
            printf("%s ", tok);
          }
          printf("\n");
          break;
        case ';' : /* end of expression, skip to next expression. */
          break;
        case '+' : /* increment the parameter and run */
        case '=' : /* increment the parameter and run */
          while ( tok[rep_command] == '+' || tok[rep_command] == '=')
          {
            rep_command++;
          }
          sscanf(tok+1,"%d",&rep_command); /* rep_command default value 1 */
          mu.value[p] *= pow( get_dou("parstep"), rep_command );
          printf("  %s = %s%f%s\n",mu.name[p],T_VAL,mu.value[p],T_NOR);
          sim_status |= SS_MODIFIED_RUN;
          update_act_par_options(p, mu);
          break;
        case '-' : /* decrement the parameter and run */
        case '_' : /* decrement the parameter and run */
          while ( tok[rep_command] == '-' )
          {
            rep_command++;
          }
          sscanf(tok+1,"%d",&rep_command);
          mu.value[p] /= pow( get_dou("parstep"), rep_command );
          printf("  %s = %s%f%s\n",mu.name[p],T_VAL,mu.value[p],T_NOR);
          sim_status |= SS_MODIFIED_RUN;
          update_act_par_options(p, mu);
          break;                
        case 'n' :
        case '0' : /* update/switch to normal plot */
          plot_mode = PM_NORMAL;
          sim_status |= SS_MODIFIED_PLOT;
          break;
        case 'b' :
        case '9' : /* switch to continuation plot */
          plot_mode = PM_CONTINUATION;
          sim_status |= SS_MODIFIED_PLOT;
          break;
        case 'C' :
        case '7' :
          np = getnargs(a, ntok); 
          switch (np)
          {
            case 1:
              snprintf(svalue2,EXPRLENGTH,"%s",
                getnextt(a, &ntok));
              set_str("timeframe",svalue2);
              break;
            case 2:
              snprintf(svalue2,EXPRLENGTH,"%s %s",
                getnextt(a, &ntok),getnextt(a, &ntok));
              set_str("timeframe",svalue2);
              break;
            case 0:
            default:
              break;
          }
          plot_mode = PM_PARTICLES;
          sim_status |= SS_MODIFIED_PLOT;
          break;
        case '6' :
          plot_mode = PM_ANIMATE;
          if ( (tok = getnextt(a, &ntok)) != NULL )   
          {
            tok++;
            tok[strcspn(tok,"\"")] = '\0';
            snprintf(plot_animate_string,EXPRLENGTH,"%s",tok);
          }
          else
          {
            plot_animate_string[0] = '\0';
          }

          sim_status |= SS_MODIFIED_PLOT;
          break;
        case 'r' : /* switch to replot mode */
          break;
        case 'R' :
          sim_status |= SS_MODIFIED_RUN;
          break;
        case 'h' : /* toggle hold */
          /* hold to hold the current plot 
           * and keep the same simulation
           * there is another 'curves' option
           * to keep track of the last simulations
           * with curve.0, curve.1 etc.
           *
           * hold and curves are mutually exclusive
           * hold is meant not to run the simulation again while
           * curves is. 
           */
          if ( tok[1] == 'd' ) /* delay switching on of hold to after next plot */
          {
            set_str("hold","delay");
            set_int("hold",0);
          }
          else 
          {
            set_int("hold",1-get_int("hold"));
          }
          if ( get_int("hold") )
          {
            set_int("curves",0); /* unset curves */
            printf("  %shold is on%s\n",T_DET,T_NOR);
          }
          else if ( strncmp(get_str("hold"),"delay", 5) == 0 )
          {
            printf("  %shold is delayed%s\n",T_DET,T_NOR);
          }
          else
          {
            printf("  %shold is off%s\n",T_DET,T_NOR);
          }
          break;
        case 'u' : /* add curves on the plot */ 
          plot_mode = PM_UNDEFINED;
          if ( (tok = getnextt(a, &ntok) ) == NULL )
          {
            set_int("curves",1-get_int("curves"));
            if ( get_int("curves") )
            {
              set_int("hold",0); /* unset hold */
              printf("  %sadd curves is on%s\n",T_DET,T_NOR);
              plot_mode = PM_CURVES;
            }
            else
            {
              printf("  %sadd curves is off%s\n",T_DET,T_NOR);
            }
          }
          else if ( *tok == 'c' || *tok == 'r' ) /* try to clear or reset curves */
          {
            system("rm -f " ODEXPDIR "curve.*");
            nbr_hold = 0;
            set_int("curves",0);
            printf("  %sadd curves is off%s\n",T_DET,T_NOR);
          }
          else
          {
            PRINTERR("  %sUnknown command %s",T_ERR,T_NOR);
          }
          break;
        case '>' : /* increase resolution */
          while ( tok[rep_command] == '>' )
          {
            rep_command++;
          }
          sscanf(tok+1,"%d",&rep_command); /* rep_command default value 1 */
          set_int("res",powl(2,rep_command)*(get_int("res")-1)+1);
          sim_status |= SS_MODIFIED_RUN;
          break;
        case '<' : /* decrease resolution */
          while ( tok[rep_command] == '<' )
          {
            rep_command++;
          }
          sscanf(tok+1,"%d",&rep_command); /* rep_command default value 1 */
          set_int("res",(get_int("res")-1)/powl(2,rep_command)+1);
          sim_status |= SS_MODIFIED_RUN;
          break;
        case '!' : /* pass a command to the shell */
          printf("\n");
          joinargs(svalue, EXPRLENGTH, a, &ntok, " ");
          printf("  %s\n",svalue);
          system(svalue); 
          break;
        case 'e' : /* extend the simulation */
          if ( (tok = getnextt(a, &ntok)) != NULL )
          {
            tspan.array[tspan.length-1] = tspan.array[0] + strtod(tok,NULL) \
                      *(tspan.array[tspan.length-1]-tspan.array[0]);
          }
          else
          {
            tspan.array[tspan.length-1] += tspan.array[tspan.length-1]-tspan.array[0];
          }
          sim_status |= SS_MODIFIED_RUN;
          break;
        case 'E' : /* shorten the simulation */
          tspan.array[tspan.length-1] -= (tspan.array[tspan.length-1]-tspan.array[0])/2;
          sim_status |= SS_MODIFIED_RUN;
          break;
        case 'a' : /* set axis scale  */
          sscanf(tok+1,"%c%c",&op,&op2);
          if ( (op  == 'a' && op2 == 'l') || (op2  == 'a' && op == 'l') )
          {
            set_str("xscale","log");
            set_str("yscale","log");
            set_str("zscale","log");
          }
          if ( (op  == 'z' && op2 == 'l') || (op2  == 'z' && op == 'l') )
          {
            set_str("zscale","log");
          }
          if ( (op  == 'z' && op2 == 'n') || (op2  == 'z' && op == 'n') )
          {
            set_str("zscale","linear");
          }
          if ( (op  == 'y' && op2 == 'l') || (op2  == 'y' && op == 'l') )
          {
            set_str("yscale","log");
          }
          if ( (op  == 'y' && op2 == 'n') || (op2  == 'y' && op == 'n') )
          {
            set_str("yscale","linear");
          }
          if ( (op  == 'x' && op2 == 'l') || (op2  == 'x' && op == 'l') )
          {
            set_str("xscale","log");
          }
          if ( (op  == 'x' && op2 == 'n') || (op2  == 'x' && op == 'n') )
          {
            set_str("xscale","linear");
          }
          break;
        case 'A' : /* reset axis scales to normal */
          set_str("xscale","linear");
          set_str("yscale","linear");
          set_str("zscale","linear");
          break;
        case 'v' : /* set 2D or 3D view */
          plot_mode = PM_NORMAL;
          sim_status |= SS_MODIFIED_PLOT;
          np = getnargs(a, ntok); 
#ifdef MYDEBUG
  DBPRINT("nargs = %d (tok = %s)\n",np,tok);
#endif
          if ( np < 2 || np > 5 )
          {
            PRINTERR("  Error: Requires 2 or 3 variable names/indices");
            break;
          }
          tok = getnextt(a, &ntok); /* np >= 2, this is not NULL */
          if ( !(isdigit(*tok) || *tok == '-') )
          {
            set_str("x",tok);
            set_str("y",getnextt(a, &ntok));
            if ( np == 3 || np == 5 ) /* expect 3 names */
              set_str("z",getnextt(a, &ntok));
            update_plot_index(&pi, dxv);
            update_plot_options(pi,dxv); 
          }
          else
          {
            pi.nx = strtol(tok,NULL,10);
            pi.x = pi.nx + 2;
            pi.ny = strtol(getnextt(a, &ntok),NULL,10);
            pi.y = pi.ny + 2;
            if ( np == 3 || np == 5 ) /* expect 3 indices */
            {
              pi.nz = strtol(getnextt(a, &ntok),NULL,10);
              pi.z = pi.nz + 2;
            }
          }

          if ( np == 2 ) /* plot 2D */
          {
            set_int("plot3d",0);
          }
          if ( (pi.nx >= -1) && pi.nx < (int)total_nbr_x)
          {
            pi.x = pi.nx + 2;
          }
          else /* plot 3D */
          {
            PRINTERR("  Error: x-axis index out of bound");
          }
          if ( (pi.ny >= -1) && pi.ny < (int)total_nbr_x)
          {
            pi.y = pi.ny + 2;
          }
          else
          {
            PRINTERR("  Error: y-axis index out of bound");
          }
          if ( np == 3 )
          {
            if ( (pi.nz >= -1) && pi.nz < (int)total_nbr_x)
            {
              pi.z = pi.nz + 2;
              set_int("plot3d",1);
            }
            else
            {
              PRINTERR("  error: z-axis index out of bound");
            }
          }
          update_plot_options(pi,dxv); 
          update_plot_index(&pi, dxv);
          break;
        case 'V' : /* use gnuplot syntax to plot 2D/3D */
          plot_mode = PM_FUN;
          sim_status |= SS_MODIFIED_PLOT;
          if ( (tok = getnextt(a, &ntok)) != NULL )
          {
            trim(tok,'"');
            snprintf(plot_str_arg,EXPRLENGTH,"%s",tok);
          }
          break;
        case 'x' :
          if ( ( tok = getnextt(a, &ntok) ) == NULL )
          {
            PRINTERR("  error: one mandatory argument");
            break;
          }
          plot_mode = PM_NORMAL;
          sim_status |= SS_MODIFIED_PLOT;
          if ( !(isdigit(*tok) || *tok == '-') ) 
          {
            set_str("x",tok);
            update_plot_index(&pi, dxv);
            pi.x = pi.nx + 2;
            set_int("plot3d",0);
            update_plot_options(pi,dxv); 
          }
          else 
          {
            pi.nx = strtol(tok,NULL,10);
            if (pi.nx >= -1 && pi.nx < (int)total_nbr_x)
            {
              pi.x = pi.nx + 2;
              set_int("plot3d",0);
              update_plot_options(pi,dxv); 
              update_plot_index(&pi, dxv);
            }
            else 
            {
              PRINTERR("  Error: Variable index out of bound");
            }
          }
          break;
        case 'y' :
          if ( ( tok = getnextt(a, &ntok) ) == NULL )
          {
            PRINTERR("  error: one mandatory argument");
            break;
          }
          plot_mode = PM_NORMAL;
          sim_status |= SS_MODIFIED_PLOT;
          if ( !(isdigit(*tok) || *tok == '-') ) 
          {
            set_str("y",tok);
            update_plot_index(&pi, dxv);
            pi.nx = -1;
            pi.x = pi.nx + 2;
            pi.y = pi.ny + 2;
            set_int("plot3d",0);
            update_plot_options(pi,dxv); 
          }
          else 
          {
            pi.ny = strtol(tok,NULL,10);
            if (pi.ny >= -1 && pi.ny < (int)total_nbr_x)
            {
              pi.nx = -1;
              pi.x = pi.nx + 2;
              pi.y = pi.ny + 2;
              set_int("plot3d",0);
              update_plot_options(pi,dxv); 
              update_plot_index(&pi, dxv);
            }
            else 
            {
              PRINTERR("  Error: Variable index out of bound");
            }
          }
          break;
        case ']' : /* plot next x */
          plot_mode = PM_NORMAL;
          sim_status |= SS_MODIFIED_PLOT;
          pi.ny=pi.y-2;
          pi.ny++;
          pi.ny %= (int)total_nbr_x;
          pi.y = pi.ny+2;
          update_plot_options(pi,dxv); 
          update_plot_index(&pi, dxv);
          printf("  y-axis: [%s%d%s] %s\n",T_IND,pi.ny,T_NOR,dxv.name[pi.ny]);
          break;
        case '[' : /* plot previous x */
          plot_mode = PM_NORMAL;
          sim_status |= SS_MODIFIED_PLOT;
          pi.ny = pi.y-2;
          pi.ny+= (int)total_nbr_x-1;
          pi.ny %= (int)total_nbr_x;
          pi.y = pi.ny+2;
          update_plot_options(pi,dxv); 
          update_plot_index(&pi, dxv);
          printf("  y-axis: [%s%d%s] %s\n",T_IND,pi.ny,T_NOR,dxv.name[pi.ny]);
          break;    
        case '}' : /* plot next particle  */
          plot_mode = PM_NORMAL;
          sim_status |= SS_MODIFIED_PLOT;
          if ( SIM->max_id )
          {
            set_int("particle", (get_int("particle") + 1) % SIM->max_id);
          }
          else
          {
            set_int("particle", 0);
          }
          break;
        case '{' : /* plot previous particle  */
          plot_mode = PM_NORMAL;
          sim_status |= SS_MODIFIED_PLOT;
          if ( SIM->max_id )
          {
            set_int("particle", (get_int("particle") + SIM->max_id - 1) % SIM->max_id);
          }
          else
          {
            set_int("particle", 0);
          }
          break;    
        case 'i' : /* run with initial conditions */
          if ( sscanf(tok+1,"%c",&op) )
          {
            if ( op == 'l' ) /* last simulation value */
            {
              set_int("init",1);
            } 
            else if ( op == 's') /* run from steady state */
            {
              printf("  not functional\n");
            }
            else if ( op == 'a' ) /* get ic from arpn. */
            {
              set_int("init", 2);
              sscanf(rawcmdline+2,"%[^;\n]",svalue);
              set_str("init", svalue);
            }
            sim_status |= SS_MODIFIED_RUN;
          }
          break;
        case 'I' : /* set initial condition to defaults */
          set_int("init", 0);  /* no last y IC */
          tok = strdup("li");
          ntok--;
          sim_status |= SS_MODIFIED_RUN;
          break;
        case 't':
          if ( getnargs(a, ntok) == 0 )
          {
            /* list tspan */
            printf("  tspan = "); 
            for(i=0;i<tspan.length;i++)
            {
              printf("%s%g%s ",T_VAL,tspan.array[i],T_NOR);
            }
            printf("\n");
            break; /* do not update timespan */
          } 
          free(tspan.array);
          tspan.length = getnargs(a,ntok); 
          tspan.array = malloc(tspan.length*sizeof(double));
          for(i = 0; i < tspan.length; i++)
          {
            tok = getnextt(a, &ntok); 
            if ( (isdigit(*tok) || *tok == '-') ) 
            { 
              tspan.array[i] = strtod(tok,NULL);
            }
            else
            { 
              tspan.array[i] = NAN; 
            }
          }
          if ( tspan.length < 2 )
          {
            PRINTERR("  Error: tspan is length %d, but should be at least of length 2.",tspan.length);
          }
          else if ( tspan.array[tspan.length-1] < tspan.array[0] )
          {
            PRINTERR("  Error: End time point %g should be greater than t0 %g.", \
                tspan.array[tspan.length-1], tspan.array[0]);
          }   
          break;
        case '@':
          if ( getnargs(a, ntok) == 0 )
          {
            /* list range_array */
            printf("  range array = "); 
            for(i=0;i<range_array.length;i++)
            {
              printf("%s%g%s ",T_VAL,range_array.array[i],T_NOR);
            }
            printf("\n");
            break; /* do not update range_array */
          } 
          free(range_array.array);
          range_array.length = getnargs(a,ntok); 
          range_array.array = malloc(range_array.length*sizeof(double));
          for(i = 0; i < range_array.length; i++)
          {
            tok = getnextt(a, &ntok); 
            if ( (isdigit(*tok) || *tok == '-') ) 
            { 
              range_array.array[i] = strtod(tok,NULL);
            }
            else
            { 
              range_array.array[i] = NAN; 
            }
          }
          break;
        case 'l' : 
          tok++;
          switch(*tok)
          {
            case 'p': /* list parameters */
              for (i=0; i<mu.nbr_el; i++)
              {
                padding = (int)log10(mu.nbr_el+0.5)-(int)log10(i+0.5);
                if ( i == p ) /* listing active parameter */
                {
                  snprintf(par_details,17,"active parameter");  
                }
                else
                {
                  par_details[0] = 0; /* snprintf(par_details,1,"");   */
                }
                printf_list_val('P',i,i,padding,&mu,par_details);
              }
              break;
            case 'i': /* list initial conditions */
              for (i=0; i<ode_system_size; i++)
              {
                ics.value[i] = NAN;
                if ( strncmp(ics.attribute[i],"hidden",3) )
                {
                  padding = (int)log10(ics.nbr_el+0.5)-(int)log10(i+0.5);
                  /* DBPRINT("update initial condition values for particle"); */
                  if (get_int("init") == 0)
                  {
                    printf_list_str('I',i,i,padding,&ics);
                  }
                  else
                  {
                    snprintf(list_msg,EXPRLENGTH,"numerically set (I to revert to %s)",ics.expression[i]);
                    printf_list_val('I',i,i,padding,&ics,list_msg);

                  }
                }
              }
              break; 
            case 'x': /* list equations */
              if ( (tok = getnextt(a, &ntok)) == NULL ) 
              {
                tok = dacme; 
              }
              if ( strchr(tok,'D') != NULL )
              {
                for (i=0; i<eqn.nbr_el; i++)
                {
                  if ( strstr(eqn.attribute[i],"hidden") == NULL )
                  {
                    padding = (int)log10(total_nbr_x+0.5)-(int)log10(i+0.5);
                    printf_list_str('D',i,i,padding,&eqn);
                  }
                }
              }
              if ( strchr(tok,'A') != NULL )
              {
                for (i=0; i<fcn.nbr_el; i++)
                {
                  if ( strstr(fcn.attribute[i],"hidden") == NULL )
                  {
                    padding = (int)log10(total_nbr_x+0.5)-(int)log10(i+eqn.nbr_el+0.5);
                    printf_list_str('A',i+eqn.nbr_el,i,padding,&fcn);
                  }
                }
              }
              if ( strchr(tok,'C') != NULL )
              {
                for (i=0; i<psi.nbr_el; i++)
                {
                  if ( strstr(psi.attribute[i],"hidden") == NULL )
                  {
                    padding = (int)log10(total_nbr_x+0.5)-(int)log10(i+eqn.nbr_el+fcn.nbr_el+0.5);
                    printf_list_str('C',i+eqn.nbr_el+fcn.nbr_el,i,padding,&psi);
                  }
                }
              }
              if ( strchr(tok,'M') != NULL )
              {
                for (i=0; i<mfd.nbr_el; i++)
                {
                  if ( strstr(mfd.attribute[i],"hidden") == NULL )
                  {
                    padding = (int)log10(total_nbr_x+0.5)-\
                              (int)log10(i+eqn.nbr_el+fcn.nbr_el+psi.nbr_el+0.5);
                    printf_list_str('M',i+eqn.nbr_el+fcn.nbr_el+psi.nbr_el,i,padding,&mfd);
                  }
                }
              }
              if ( strchr(tok,'E') != NULL )
              {
                for (i=0; i<pex.nbr_el; i++)
                {
                  if ( strstr(pex.attribute[i],"hidden") == NULL )
                  {
                    padding = (int)log10(total_nbr_x+0.5)-\
                              (int)log10(i+eqn.nbr_el+fcn.nbr_el+psi.nbr_el+mfd.nbr_el+0.5);
                    printf_list_str('E',i+eqn.nbr_el+fcn.nbr_el+psi.nbr_el+mfd.nbr_el,i,padding,&pex);
                  }
                }
              }
              break;
            case 'c': /* list constant arrays*/ 
              for (i=0; i<cst.nbr_el; i++)
              {
                padding = (int)log10(cst.nbr_el+0.5)-(int)log10(i+0.5);
                printf_list_str('C',i,i,padding,&cst);
              }
              break;
            case 'D': /* list data files */ 
              for (i=0; i<dfl.nbr_el; i++)
              {
                padding = (int)log10(dfl.nbr_el+0.5)-(int)log10(i+0.5);
                printf_list_str('F',i,i,padding,&dfl);
              }
              break;
            case 'F': /* list user-defined functions */ 
              for (i=0; i<func.nbr_el; i++)
              {
                padding = (int)log10(func.nbr_el+0.5)-(int)log10(i+0.5);
                printf_list_str('F',i,i,padding,&func);
              }
              break;
            case '%': /* list birth/repli/death rates */ 
              padding = 0;
              if ( birth.nbr_el > 0)
              {    
                printf_list_str('%',0,0,padding,&birth);
              }
              if ( repli.nbr_el > 0)
              {
                printf_list_str('%',0,0,padding,&repli);
              }
              if ( death.nbr_el > 0)
              {
                printf_list_str('%',0,0,padding,&death);
              }
              break;
            case 's': /* list steady states */
              for (j=0; j<nbr_stst; j++)
              {
                for (i=0; i<ode_system_size; i++)
                {
                  padding = (int)log10(ics.nbr_el+0.5)-(int)log10(i+0.5);
                  printf("  S[%s%d%s]%-*s %-*s = %s%14g%s   %s%s%s\n",\
                      T_IND,i,T_NOR, padding, "",*ics.max_name_length,ics.name[i],\
                      T_VAL,stst[j].s[i],T_NOR,T_DET,"*",T_NOR);
                }
                printf("  *status: %s%s%s\n",T_DET,gsl_strerror(stst[j].status),T_NOR);
              }
              break;
            case 'o': /* list options */
              printf_options(getnextt(a, &ntok));
              break;
            case 'l': /* list fiLe and various information */
              printf("  File name: %s%s%s\n",T_EXPR,odexp_filename,T_NOR);
              printf("  odexp directory: " ODEXPDIR "\n");
              printf("  Number of dynamical variables = %s%d%s\n",T_VAL,ode_system_size,T_NOR);
              printf("  Number of auxiliary functions = %s%d%s\n",T_VAL,fcn.nbr_el,T_NOR);
              printf("  Number of coupling terms      = %s%d%s\n",T_VAL,psi.nbr_el,T_NOR);
              printf("  Number of mean fields         = %s%d%s\n",T_VAL,mfd.nbr_el,T_NOR);
              printf("  Number of param. expressions  = %s%d%s\n",T_VAL,pex.nbr_el,T_NOR);
              printf("  Number of plottable variables = %s%d%s\n",T_VAL,total_nbr_x,T_NOR);
              printf("  Number of parameters          = %s%d%s\n",T_VAL,mu.nbr_el,T_NOR);
              printf("  Number of constants           = %s%d%s\n",T_VAL,cst.nbr_el,T_NOR);
              printf("  Number of data files          = %s%d%s\n",T_VAL,dfl.nbr_el,T_NOR);
              printf("  Number of columns in id.dat   = %s%d%s\n",T_VAL,nbr_col,T_NOR);
              printf("  Initial population size       = %s%d%s\n",T_VAL,(int)get_int("popsize"),T_NOR);
              printf("  Final population size         = %s%d%s\n",T_VAL,POP_SIZE,T_NOR);
              printf("  Number of particles           = %s%d%s\n",T_VAL,SIM->max_id,T_NOR);
              printf("\n  plot mode                     = %s%d%s\n",T_VAL,(int)plot_mode,T_NOR);
              printf("\n  hexdump -e '%d \"%%5.2f \" 4 \"%%5d \" \"\\n\"' " STATS_FILENAME "\n", 1+mfd.nbr_el); 
              printf("  hexdump -e '\"%%u \" \"%%d \" %d \"%%5.2f \" \"\\n\"' " TRAJ_FILENAME "\n", nbr_col); 
              printf("  hexdump -e '\"%%d \" %d \"%%5.2f \" \"\\n\"' " PSTATE_FILENAME "\n", total_nbr_x); 
              printf("  hexdump -e '3 \"%%5.2f \" \"\\n\"' " QUICK_BUFFER "\n\n"); 
              break;
            case 'd': /* list descriptions */ 
              for (i=0; i<dsc.nbr_el; i++)
              {
                printf("  %s%s%s\n", T_DET, dsc.comment[i], T_NOR);
              }
              break;
            case '.': /* list extra commands */ 
              for (i=0; i<xcd.nbr_el; i++)
              {
                printf("  %s%s%s\n", T_DET, xcd.comment[i], T_NOR);
              }
              break;
            case 'w': /* list warning/error messages */
              printf_msg_buff();
              break;
            default: 
              PRINTERR("  Error: Unknown command. Cannot list '%c'",*tok);
          }
          break;
        case 'p' : /* change current parameter */
          if ( (tok = getnextt(a, &ntok)) != NULL ) /* get parameter name/index */
          {
            if ( !(isdigit(*tok) || *tok == '-') ) 
            {
              err = name2index(tok,mu,&p);
            }
            else
            {
              np = strtol(tok,NULL,10);
              if (np > -1 && np < mu.nbr_el) /* new active parameter */
              {
                err = 0;
                p = np;
              }
              else
              {
                err = 1;
                PRINTERR("  Error: Parameter index out of bound. Use lp to list parameters");
                printf("  (the active parameter %s = %s%lg)%s\n", mu.name[p],T_VAL,mu.value[p],T_NOR);
              }
            }
            if ( err == 0 )
            {
              if ( (tok = getnextt(a, &ntok)) != NULL ) /* get optional parameter value */
              {
                nvalue = strtod(tok,NULL);
                if ( errno != EINVAL )
                {
                  mu.value[p] = nvalue;
                  sim_status |= SS_MODIFIED_RUN;
                  printf("  active parameter %s set to %s%lg%s\n", mu.name[p],T_VAL,mu.value[p],T_NOR);
                }
                else
                {
                  printf("  could not read new value %s for parameter %s, (%s = %s%lg%s)\n", \
                      tok, mu.name[p], mu.name[p], T_VAL,mu.value[p],T_NOR);
                }
              }
              else
              {
                printf("  new active parameter %s with value %s%lg%s\n", mu.name[p],T_VAL,mu.value[p],T_NOR);
              }
            }
          } 
          else
          {
            printf("  active parameter %s is %s%lg%s\n", mu.name[p],T_VAL,mu.value[p],T_NOR);
          }
          update_act_par_options(p, mu);
          break;
        case 'P' : /* set value of current parameter */
          if ( (tok = getnextt(a, &ntok)) != NULL )  
          {
            mu.value[p] = strtod(tok,NULL);
            printf("  %s = %s%lg%s\n", mu.name[p],T_VAL,mu.value[p],T_NOR);
            sim_status |= SS_MODIFIED_RUN;
          }
          else
          {
            PRINTERR("  Error: Expected a numerical parameter value (double)");
          }
          update_act_par_options(p, mu);
          break;
        case 's' : /* set options/view options */
          if ( strncmp(tok,"set",3) == 0 ) /* change options */
          {
            if ( (np = getnargs(a, ntok)) ) /* at least one argument */
            {
              tok = getnextt(a, &ntok);
              if ( !(isdigit(*tok)) ) 
              {
                option_name2index(tok, &i);
              }
              else
              {
                i = strtol(tok,NULL,10);
                if ( errno == EINVAL )
                {
                  /* TODO error */
                }
              }
              if ( np == 1 && i < NBROPTS )
              {
                printf_option_line(i);
                break;
              }
              if ( i >= NBROPTS )
              {
                PRINTERR("  Error: Option index out of bound");                
                break; 
              }
              if ( np > 1 && i < NBROPTS )
              {
                joinargs(svalue, EXPRLENGTH, a, &ntok, " ");
                switch (GOPTS[i].valtype)
                {
                  case 'd':
                    GOPTS[i].numval = strtod(svalue,NULL);
                    break;
                  case 'i':
                    GOPTS[i].intval = strtol(svalue,NULL,10);
                    break;
                  case 's':
                    /* trim(svalue,'"'); */
                    strncpy(GOPTS[i].strval,svalue,NAMELENGTH);
                    break;
                  default:
                    PRINTWARNING("  warning: option not defined\n");
                }
                update_plot_index(&pi, dxv);
                update_act_par_index(&p, mu);
                printf_option_line(i);
                gnuplot_config();
                break;
              }
            }
          }
          else
          {    
            PRINTERR("  Error: unknown command %s", tok);
          }
          break;
        case '?' : /* help */
          HELPCMD;
          break;
        case 'd' : /* reset parameters and initial cond to defaults */
          set_int("init", 0);
          /* reset parameter values */
          load_nameval(PAR_FILENAME, mu, "PAR", 3,EXIT_IF_NO_FILE);
          sim_status |= SS_MODIFIED_RUN;
          update_act_par_options(p, mu);
          break;
        case 'o' : /* open a parameter file */
#ifdef MYDEBUG
  DBPRINT("Probably a lot to fix here");
#endif
          nbr_read = sscanf(rawcmdline+2,"%s",new_par_filename);
          if ( nbr_read < 1 )
          {
            PRINTERR("  Error: Name of parameter file missing.");
          }
          else /* read new_par_filename for parameters, initial conditions, and tspan */
          {
            /* load parameter values */
            err = load_nameval(new_par_filename, mu, "PAR", 3,DONT_EXIT_IF_NO_FILE);
            if ( err )
            {
              PRINTWARNING("  warning: could not load parameters.\n");
            }
            err = load_nameval(new_par_filename, ics, "INIT", 4,DONT_EXIT_IF_NO_FILE); /* load initial conditions value from file */
            if ( err == 0 )
            {
              /* reset initial condtitions */
              set_int("init", 1);
            }
            else
            {
              PRINTWARNING("  warning: could not load initial conditions.\n");
            }
            load_double_array(new_par_filename, &tspan, ts_string, ts_len, DONT_EXIT_IF_NO_FILE); 
            if ( tspan.length == 0 )
            {
              PRINTWARNING("  warning: could not load tspan.\n");
            }
            err = load_options(new_par_filename, DONT_EXIT_IF_NO_FILE);
            if ( err )
            {
              PRINTWARNING("  warning: could not load options.\n");
            }
          }
          sim_status |= SS_MODIFIED_RUN;
          break;
        case ':' : /* issue a gnuplot command */
        case 'g' : /* issue a gnuplot command */
          fprintf(GPLOTP,"%s\n", rawcmdline+1);
          fflush(GPLOTP);
          plot_mode = PM_UNDEFINED;
          break;
        case 'm' :
          sscanf(tok+1,"%c",&op);
          /* first clear stst */
          if ( nbr_stst > 0 )
          {
            free_steady_state(stst, nbr_stst);
          }
          nbr_stst = 0;
          stst = NULL;
          if ( op  == 's') /* compute steady state */
          {
            /* init steady state */
            stst = malloc(sizeof(steady_state));
            init_steady_state( &(stst[0]), 0 );
            nbr_stst = 1;
            DBPRINT("finding steady state for particle %d", SIM->pop->start->id);
            ststsolver(root_rhs,SIM->pop->start->y,SIM->pop->start,stst);
          } 
          else if ( op == 'm')
          {
            nbr_stst = phasespaceanalysis(root_rhs,SIM->pop->start->y,SIM->pop->start, &stst);
            for (j=0; j<nbr_stst; j++)
            {
              printf("  *status: %s%s%s\n",T_DET,gsl_strerror(stst[j].status),T_NOR);
            }
          } 
          else if ( op == 'c' )
          {
            ststcont(root_rhs,ics,SIM->pop->start);
            plot_mode = PM_CONTINUATION;
          }
          break;
        case '#' : /* add data from file to plot */
          if ( (tok = getnextt(a, &ntok) ) != NULL ) /* got the dataset name */
          {
            i = 0;
            while ( (i<dfl.nbr_el) && strcmp(tok,dfl.name[i]) )
            {
              i++;
            }
            if (i<dfl.nbr_el) /* found the dataset to plot */
            {
              if ( sscanf(dfl.expression[i]," %s ",data_fn) )
              {
                set_str("data2plot", dfl.name[i]);
              }
            }
            if ( (tok = getnextt(a, &ntok) ) != NULL ) 
            {
                trim(tok,'"');
                strncpy(plot_str_arg,tok,EXPRLENGTH);
            }
            sim_status |= SS_ACTION_DATA;
            sim_status |= SS_MODIFIED_PLOT;
          }
          else 
          {
            printf("  Dataset not found. plotting data off\n");
            plot_mode = PM_UNDEFINED;
          }
          break;
        case 'W' : /* wait (seconds) */
          if ( (tok = getnextt(a, &ntok)) != NULL )  
          {
            rqtp.tv_nsec = 1000000*strtol(tok,NULL,10); /* get waiting time in milliseconds */
            rqtp.tv_sec = 0.0;
            nanosleep(&rqtp,NULL);
          }
          break;
        case 'w' :  /* world */
          printf_SIM();
          break;
        case 'S' :  /* list particle dataset */
          if ( (*(tok + 1) > 47) && (*(tok + 1) < 58)  )
          {
            sscanf(tok+1,"%d",&i); /* list a single particle */
            list_particle(i);
          }
          else if ( sscanf(tok+1,"%c",&op ) == 1 )
          {
            if ( (op == 'S') | (op == 'a') | (op == '.') | (op == '_') ) /* list all trajectories */
            {
              list_traj(); 
            }
          }
          else if ( (tok = getnextt(a, &ntok) ) != NULL )
          {
            list_particle(strtol(tok,NULL,10)); 
          }
          else
          {
            list_stats();
          }
          break;
        case 'Q' :  /* quit with without save */
          sim_status = SS_ACTION_QUIT;
          break;
        case 'q' :  /* quit with save */
          sim_status = SS_ACTION_QUIT;
          /* fall through */
        case '*' : /* save snapshot of pop file */
          save_snapshot(ics,mu,tspan, odexp_filename);
          break;
        default :
          PRINTERR("  Unknown command '%c'. Type q to quit, ? for help", *tok);
			}
			if ( sim_status & SS_ACTION_QUIT )
				break;

      if ( tok != NULL && *tok != ';' ) /* if there are remaining tokens, and if it is not end of command ';' 
                                           do the following checks */
      {
        /* check for extra arguments */
        if ( (tok = getnextt(a, &ntok) ) != NULL ) 
          if ( strcmp(tok,"title") == 0 )
            get_plottitle(getnextt(a, &ntok));
        /* move to end of command */
      }
      while ( getnextt(a, &ntok) != NULL )
        ; /* advance to end of command a.array[ntok] == ';' or ntok == a.length-1 */
      ntok++;

      check_options();

#ifdef MYDEBUG
      DBPRINT("plot mode = %d", plot_mode);
#endif

      /* what to do if parameters are modified */
      if ( ( sim_status & SS_MODIFIED_RUN ) &&  get_int("run_on_modified") )
      {
          sim_status |= SS_ACTION_RUN;
      }
      if ( get_int("plot_after") )
      {
        sim_status |= SS_ACTION_PLOT;
      }

#ifdef MYDEBUG
      DBPRINT("sim_status = %d", sim_status);
#endif

      /* RUN */
      if ( sim_status & SS_ACTION_RUN )
      {
        if ( get_int("reseed") )
        {
          srand( (unsigned long)get_int("seed") );
        }
        odesolver(pop_ode_rhs, single_rhs, pop_ode_ic, single_ic, &tspan);
      }

      /* PROCESS SIMULATION DATA */
      switch(plot_mode) 
      {
        case PM_NORMAL: /* normal plot mode */
          generate_particle_file(get_int("particle")); /* generate id.dat file for the current particle */
          break;
        case PM_CURVES:
          update_curves(&nbr_hold, plotkeys);
          break;
        case PM_CONTINUATION:
          break;
        case PM_ANIMATE:
          break;
        case PM_PARTICLES:
          generate_particle_states();
          break;
        default:
          break;
      }

      /* PLOTTING */
      if ( sim_status & SS_ACTION_PLOT  )
      {
        if ( strncmp("log",get_str("xscale"),3)==0 )
        {
          fprintf(GPLOTP,"set logscale x\n");   
        }
        else
        {
          fprintf(GPLOTP,"set nologscale x\n");   
        }
        if ( strncmp("log",get_str("yscale"),3)==0 )
        {
          fprintf(GPLOTP,"set logscale y\n");   
        }
        else
        {
          fprintf(GPLOTP,"set nologscale y\n");   
        }
        if ( strncmp("log",get_str("zscale"),3)==0 )
        {
          fprintf(GPLOTP,"set logscale z\n");   
        }
        else
        {
          fprintf(GPLOTP,"set nologscale z\n");   
        }
        fflush(GPLOTP);

        switch(plot_mode) 
        {
          case PM_UNDEFINED:
            /* do nothing */
            break;

          case PM_CURVES:
            /* plot curve.0 to curve.nbr_hold-1 */
            if ( nbr_hold == 1 )
            {
              fprintf(GPLOTP,\
                  "plot \"" ODEXPDIR "curve.0\" binary format=\"%%3lf\" using 1:2 with %s title \"%s\"\n",get_str("style"),plotkeys[0]);
            }
            else if ( nbr_hold > 1 )
            {
              fprintf(GPLOTP,\
                  "replot \"" ODEXPDIR "curve.%d\" binary format=\"%%3lf\" using 1:2 with %s title \"%s\"\n",\
                  nbr_hold-1,get_str("style"),plotkeys[nbr_hold-1]);
            }
            fflush(GPLOTP);
            break;

          case PM_NORMAL: /* normal plot mode */
            /* This is where the plot is normally updated */
            if ( ! ( sim_status & SS_MODIFIED_PLOT ) )
            {
              fprintf(GPLOTP,"replot\n");
              fflush(GPLOTP);
            }
            else
            {
              setup_pm_normal(pi, get_int("plot3d"), dxv);
              gplot_normal(pi, get_int("plot3d"), dxv);
            }
            break;

          case PM_FUN: /* plot function of variables using gnuplot syntax */
            gplot_fun(plot_str_arg);
            break;

          case PM_ANIMATE:
            gplot_animate(pi, get_int("plot3d"), dxv, plot_animate_string);
            break;

          case PM_CONTINUATION: /* try to plot continuation branch */
            fprintf(GPLOTP,"set xlabel '%s'\n",mu.name[p]);
            fprintf(GPLOTP,"set xrange[%lf:%lf]\n",get_dou("par0"),get_dou("par1"));
            fprintf(GPLOTP,"plot \"stst_branches.tab\" u 2:%d w %s\n",pi.y+1,get_str("style"));
            fflush(GPLOTP);
            break;

          case PM_PARTICLES:
            if ( ! (sim_status & SS_MODIFIED_PLOT) )
            {
              fprintf(GPLOTP,"replot\n");
              fflush(GPLOTP);
            }
            else
            {
              gplot_particles(pi, dxv );
            }
            break;

          default: 
            break;
        }

        if ( sim_status & SS_ACTION_DATA )
        {
          gplot_data(plot_str_arg, data_fn);
        }

        if ( strncmp(get_str("hold"), "delay", 5) == 0 )
        {
          set_str("hold","");
          set_int("hold",1);
        }
      }

      /* update option x, y, z */
      update_plot_options(pi,dxv); 
      update_plot_index(&pi, dxv);
      /* system(postprocess); */
      update_act_par_options(p, mu);
      update_act_par_index(&p, mu);

      fflush(stdin);
      rep_command = 1;
      sim_status = SS_NULL;
      
      /* execute gplot after command */
      fprintf(GPLOTP,"%s\n", get_str("after"));
      fflush(GPLOTP);

    } /* end command loop */

  } /* END MAIN LOOP */

  PRINTV("exiting...");
  PRINTLOG("Exiting");
  DBLOGPRINT("Exiting");

  pclose(GPLOTP);
  close(GPIN);
  unlink(GP_FIFONAME);

  free_namevalexp( pex );
  free_namevalexp( mu );
  free_namevalexp( ics );
  free_namevalexp( eqn );
  free_namevalexp( fcn );
  free_namevalexp( psi );
  free_namevalexp( mfd );
  free_namevalexp( birth );
  free_namevalexp( repli );
  free_namevalexp( death );
  free_namevalexp( dxv );
  free_namevalexp( cst );
  free_namevalexp( dfl );
  free_namevalexp( dsc );
  free_namevalexp( xcd );
  free_steady_state( stst, nbr_stst );
  free_double_array( tspan );
  free_double_array( range_array );
  /* free_double_array( arr ); */
  free_world(SIM);

  PRINTLOG("Memory freed");
  DBLOGPRINT("Memory freed");

  /* write history */
  if ( write_history(HISTORY_FILENAME) )

  {
    PRINTERR( "\n  Error: could not write history");
  }

  PRINTLOG("History written");
  DBLOGPRINT("History written");

  /* try to remove frozen curves */
  system("rm -f " ODEXPDIR "curve.*");
  if ( strncmp(get_str("loudness"),"loud",3) == 0 ) /* run loud mode  */
  { 
    /* try to remove idXX curves */
    remove_id_files();
    DBLOGPRINT("id files removed");
  }
  now = time(NULL);
  PRINTLOG("%s", ctime( &now )); 
  PRINTLOG("Closing log file\n");
  DBLOGPRINT("Closing log file");
  fclose(logfr);
  fclose(dblogfr);

  /* reset xterm title */
  PRINTV("\033]0;\007");

  PRINTV(" bye\n");

  return 0;

}


int setup_nve(const char fname[], const char key[], int keylen, char type, int prefix, nve *x)
{
  get_nbr_el(fname, key, keylen, &(x->nbr_el), NULL);
  alloc_namevalexp(x);
  switch (type)
  {
    case 'l':
      load_line(fname, *x, key, keylen, EXIT_IF_NO_FILE);
      break;
    case 's':
      load_strings(fname, *x, key, keylen, prefix, EXIT_IF_NO_FILE);
      break;
    case 'n':
      load_nameval(fname, *x, key, keylen, EXIT_IF_NO_FILE);
      break;
  }
  return x->nbr_el;
}

int update_act_par_index(int *p, const nve mu)
{
  char sval[NAMELENGTH];
  if ( mu.nbr_el > 0 )
  {
    strncpy(sval,get_str("actpar"),NAMELENGTH);
    if ( strlen(sval) )
    {
      name2index(sval,mu,p);
    }
  }
  else
  {
    /* do nothing */ 
  }

  return 1;
}

int update_act_par_options(const int p, const nve mu)
{   
  int s = 0;
  if ( mu.nbr_el > 0)
  {
    set_dou("actpar",mu.value[p]);
    set_int("actpar",(int)p);
    set_str("actpar",mu.name[p]);
  }
  else
  {
    set_dou("actpar",NAN);
    set_int("actpar",(int)p);
    set_str("actpar","no parameter defined");
  }
  return s;
}

int update_curves(int *nbr_hold, char plotkeys[100][EXPRLENGTH])
{
  char mv_plot_cmd[EXPRLENGTH];
  snprintf(mv_plot_cmd,EXPRLENGTH,"cp " QUICK_BUFFER " " ODEXPDIR "curve.%d",*nbr_hold);
  system(mv_plot_cmd);
  if ( strlen(get_str("plotkey") ) )
  {
    snprintf(plotkeys[*nbr_hold],EXPRLENGTH,"%s",get_str("plotkey")); 
  }
  else
  {
    snprintf(plotkeys[*nbr_hold],EXPRLENGTH,"%d",*nbr_hold); 
  }
  (*nbr_hold)++;

  return 0;
}

int check_options( void )
{
  if ( ( strncmp( get_str("popmode"), "single", 3) == 0 )  &&  ( SIM->nbr_psi || SIM->nbr_mfd ) )
  {
    PRINTWARNING("  popmode is set to single, but there are coupling and mean field terms defined. " 
        "Behavior will be undefined.\n");
  }
  return 0;
}


void initialize_readline()
{
  rl_readline_name = "odexp";
}

char ** completion_list_completion(const char *text, int start, int end)
{
  rl_attempted_completion_over = 1;
  (void)start;
  (void)end;
  return rl_completion_matches(text, completion_list_generator);
}

char * completion_list_generator(const char *text, int state)
{
  static int list_index, len;
  char *name;
  int list_len = 0;

  if (!state) {
    list_index = 0;
    len = strlen(text);
  }

  while (list_index++<NBROPTS) 
  {
    name = GOPTS[list_index-1].name;
    if (strncmp(name, text, len) == 0) {
      return strdup(name);
    }
    name = GOPTS[list_index-1].abbr;
    if (strncmp(name, text, len) == 0) {
      return strdup(name);
    }
    name = GOPTS[list_index-1].optiontype;
    if (strncmp(name, text, len) == 0) {
      return strdup(name);
    }
  }
  --list_index;
  list_len += NBROPTS;

  while (list_index<list_len+SIM->nbr_par) 
  {
    name = SIM->parnames[list_index-list_len];
    ++list_index;
    if (strncmp(name, text, len) == 0) {
      return strdup(name);
    }
  }
  list_len += SIM->nbr_par;
  while (list_index<list_len+SIM->nbr_var) 
  {
    name = SIM->varnames[list_index-list_len];
    ++list_index;
    if (strncmp(name, text, len) == 0) {
      return strdup(name);
    }
  }
  list_len += SIM->nbr_var;
  while (list_index<list_len+SIM->nbr_aux) 
  {
    name = SIM->auxnames[list_index-list_len];
    ++list_index;
    if (strncmp(name, text, len) == 0) {
      return strdup(name);
    }
  }
  list_len += SIM->nbr_aux;
  while (list_index<list_len+SIM->nbr_psi) 
  {
    name = SIM->psinames[list_index-list_len];
    ++list_index;
    if (strncmp(name, text, len) == 0) {
      return strdup(name);
    }
  }
  list_len += SIM->nbr_psi;
  while (list_index<list_len+SIM->nbr_mfd) 
  {
    name = SIM->mfdnames[list_index-list_len];
    ++list_index;
    if (strncmp(name, text, len) == 0) {
      return strdup(name);
    }
  }

  return NULL;
}



