#ifndef _OUTPUT_MANAGER_H_
#define _OUTPUT_MANAGER_H_

#include "odexp.h"
#include "datastruct.h"

extern FILE* GPLOTP;
extern int GPIN;
extern char* rawcmdline;

int gnuplot_config( );
int save_snapshot(nve init, nve mu, double_array tspan, const char *odexp_filename);
int printf_status_bar( double_array *tspan );
/* gnuplot fifo */
int read_msg( void );
int printf_msg_buff( void );

int printf_options(const char *optiontype);
void printf_list_val(char type, int print_index, int nve_index, int padding, const nve *var, char *descr);
void printf_list_str(char type, int print_index, int nve_index, int padding, const nve *var);
void printf_SIM( void );

#endif
