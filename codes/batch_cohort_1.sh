#!/bin/zsh

set -eu # makes your program exit on error or unbound variable

progname=$0

function usage {
    echo "usage: $(basename $progname) TODO:ENTER USAGE"
    exit 1
}

#############################
runcim=true
runiom=true
runfpm=true
runpdm=true
runrev=true
#############################

path_to_datasets="datasets/"
path_to_results="results/"

# set virtual cohort
ln -fs "${path_to_datasets}"virtual_cohort_1.csv virtual_cohort.csv

odexp -d codes/dynamical_models.pop  # generate code
odexp -c codes/dynamical_models.pop  # compile code

echo "opt loudness quiet" >>odexpdir/parameters.pop

# range over carbs, g_ED
cohort_size=1000
par1name="carbs"
par2name="g_ED"
par2=0.0
par1step=10.0
par2step=1
n1runs=9
n2runs=2
sed -E -i .bak 's/(par[ ]+cohort_nbr[ ]+)[^ ]+/\11/' odexpdir/parameters.pop
sed -E -i .bak 's/(par[ ]+rhoEI1[ ]+)[^ ]+/\10.0/' odexpdir/parameters.pop
sed -E -i .bak 's/(times[ ].*$)/times 0 5475/' odexpdir/parameters.pop
echo -n "MODEL,VARIANT,$par1name,$par2name," >cohort_1_two_param_range.csv
cp codes/traj_varnames.txt odexpdir/.
sed -E 's/\t/,/g' odexpdir/traj_varnames.txt >>cohort_1_two_param_range.csv

# CIM:EDM energy deposition model 
if [ "$runcim" = true ]
then
  echo "running cohort 1, CIM:EDM"
  rm -f cohort_1_edm.csv
  sed -E -i .bak 's/(par[ ]+MODEL[ ]+)[^ ]+/\11.0/' odexpdir/parameters.pop
  for n2 in {1..$n2runs} 
  do
    par1=10.0
    for n1 in {1..$n1runs}
    do
      echo "CIM:EDM; $par1name: $par1; $par2name: $par2"
      # set parameters
      sed -E -i .bak 's/(par[ ]+'"$par1name"'[ ]+)[^ ]+/\1'"$par1"'/' odexpdir/parameters.pop
      sed -E -i .bak 's/(par[ ]+'"$par2name"'[ ]+)[^ ]+/\1'"$par2"'/' odexpdir/parameters.pop
      ./model.out >/dev/null # run code

      # extract simulations at TIME = 0 (STEP 0)
      for i in {1..$cohort_size}
      do
        echo "CIM,EDM,$par1,$par2," >>temp_pars.csv
      done
      hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 0 { print }' >temp_traj.csv
      lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
      cat temp_par_traj.csv >>!cohort_1_edm.csv
      rm temp_pars.csv

      # extract simulations at TIME = 3 years(STEP 1)
      for i in {1..$cohort_size}
      do
        echo "CIM,EDM,$par1,$par2," >>temp_pars.csv
      done
      hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 1 { print }' >temp_traj.csv
      lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
      cat temp_par_traj.csv >>cohort_1_edm.csv
      rm temp_pars.csv

      # extract simulations at TIME = 5475 (STEP 5)
      for i in {1..$cohort_size}
      do
        echo "CIM,EDM,$par1,$par2," >>temp_pars.csv
      done
      hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 5 { print }' >temp_traj.csv
      lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
      cat temp_par_traj.csv >>cohort_1_edm.csv
      par1=$(($par1+$par1step))
      rm temp_pars.csv
      rm temp_par_traj.csv
      rm temp_traj.csv
    done
    par2=$(($par2+$par2step))
    echo '\n' >>cohort_1_edm.csv
  done
fi # runcim
cat cohort_1_edm.csv >>cohort_1_two_param_range.csv

# EBM
if [ "$runiom" = true ]
then
  echo "running cohort 1, EBM"
  rm -f cohort_1_iom.csv
  sed -E -i .bak 's/(par[ ]+MODEL[ ]+)[^ ]+/\10.0/' odexpdir/parameters.pop
  par1=50.0
  par2=0.0
  echo "Cohort 1; EBM; $par1name: $par1; $par2name: $par2"
  # set parameters
  sed -E -i .bak 's/(par[ ]+'"$par1name"'[ ]+)[^ ]+/\1'"$par1"'/' odexpdir/parameters.pop
  sed -E -i .bak 's/(par[ ]+'"$par2name"'[ ]+)[^ ]+/\1'"$par2"'/' odexpdir/parameters.pop
  ./model.out >/dev/null # run code

  # extract simulations at TIME = 0 (STEP 0)
  for i in {1..$cohort_size}
  do
    echo "EBM,IOM,$par1,$par2," >>temp_pars.csv
  done
  hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 0 { print }' >temp_traj.csv
  lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
  cat temp_par_traj.csv >>!cohort_1_iom.csv
  rm temp_pars.csv

  # extract simulations at TIME = 3 years (STEP 1)
  for i in {1..$cohort_size}
  do
    echo "EBM,IOM,$par1,$par2," >>temp_pars.csv
  done
  hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 1 { print }' >temp_traj.csv
  lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
  cat temp_par_traj.csv >>cohort_1_iom.csv
  rm temp_pars.csv

  # extract simulations at TIME = 5475 (STEP 5)
  for i in {1..$cohort_size}
  do
    echo "EBM,IOM,$par1,$par2," >>temp_pars.csv
  done
  hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 5 { print }' >temp_traj.csv
  lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
  cat temp_par_traj.csv >>cohort_1_iom.csv
  par1=$(($par1+$par1step))
  rm temp_pars.csv
  rm temp_par_traj.csv
  rm temp_traj.csv
fi # runiom
cat cohort_1_iom.csv >>cohort_1_two_param_range.csv

# PARTITION MODEL
if [ "$runfpm" = true ]
then
  echo "running cohort 1, CIM:APES"
  rm -f cohort_1_fpm.csv
  par1step=20.0
  n1runs=3
  par2=0.0
  sed -E -i .bak 's/(par[ ]+MODEL[ ]+)[^ ]+/\12.0/' odexpdir/parameters.pop
  for n2 in {1..$n2runs} 
  do
    par1=30.0
    for n1 in {1..$n1runs}
    do
      echo "Cohort 1; CIM:APES; $par1name: $par1; $par2name: $par2"
      # set parameters
      sed -E -i .bak 's/(par[ ]+'"$par1name"'[ ]+)[^ ]+/\1'"$par1"'/' odexpdir/parameters.pop
      sed -E -i .bak 's/(par[ ]+'"$par2name"'[ ]+)[^ ]+/\1'"$par2"'/' odexpdir/parameters.pop
      ./model.out >/dev/null # run code

      # extract simulations at TIME = 0 (STEP 0)
      for i in {1..$cohort_size}
      do
        echo "CIM,APES,$par1,$par2," >>temp_pars.csv
      done
      hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 0 { print }' >temp_traj.csv
      lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
      cat temp_par_traj.csv >>!cohort_1_fpm.csv
      rm temp_pars.csv

      # extract simulations at TIME = 3 years(STEP 1)
      for i in {1..$cohort_size}
      do
        echo "CIM,APES,$par1,$par2," >>temp_pars.csv
      done
      hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 1 { print }' >temp_traj.csv
      lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
      cat temp_par_traj.csv >>cohort_1_fpm.csv
      rm temp_pars.csv

      # extract simulations at TIME = 5475 (STEP 5)
      for i in {1..$cohort_size}
      do
        echo "CIM,APES,$par1,$par2," >>temp_pars.csv
      done
      hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 5 { print }' >temp_traj.csv
      lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
      cat temp_par_traj.csv >>cohort_1_fpm.csv
      par1=$(($par1+$par1step))
      rm temp_pars.csv
      rm temp_par_traj.csv
      rm temp_traj.csv
    done
    par2=$(($par2+$par2step))
  done
fi # runfpm
cat cohort_1_fpm.csv >>cohort_1_two_param_range.csv

# CIM FULL MODEL (APES + EDP, g_ED = 1)
if [ "$runpdm" = true ]
then
  echo "running cohort 1, CIM:PDM"
  rm -f cohort_1_pdm.csv
  par1step=20.0
  n1runs=3
  par2=0.0
  sed -E -i .bak 's/(par[ ]+MODEL[ ]+)[^ ]+/\13.0/' odexpdir/parameters.pop
  for n2 in {1..$n2runs} 
  do
    par1=30.0
    for n1 in {1..$n1runs}
    do
      echo "Cohort 1; CIM:PDM; $par1name: $par1; $par2name: $par2"
      # set parameters
      sed -E -i .bak 's/(par[ ]+'"$par1name"'[ ]+)[^ ]+/\1'"$par1"'/' odexpdir/parameters.pop
      sed -E -i .bak 's/(par[ ]+'"$par2name"'[ ]+)[^ ]+/\1'"$par2"'/' odexpdir/parameters.pop
      ./model.out >/dev/null # run code

      # extract simulations at TIME = 0 (STEP 0)
      for i in {1..$cohort_size}
      do
        echo "CIM,PDM,$par1,$par2," >>temp_pars.csv
      done
      hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 0 { print }' >temp_traj.csv
      lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
      cat temp_par_traj.csv >>!cohort_1_pdm.csv
      rm temp_pars.csv

      # extract simulations at TIME = 3 years(STEP 1)
      for i in {1..$cohort_size}
      do
        echo "CIM,PDM,$par1,$par2," >>temp_pars.csv
      done
      hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 1 { print }' >temp_traj.csv
      lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
      cat temp_par_traj.csv >>cohort_1_pdm.csv
      rm temp_pars.csv

      # extract simulations at TIME = 5475 (STEP 5)
      for i in {1..$cohort_size}
      do
        echo "CIM,PDM,$par1,$par2," >>temp_pars.csv
      done
      hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 5 { print }' >temp_traj.csv
      lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
      cat temp_par_traj.csv >>cohort_1_pdm.csv
      par1=$(($par1+$par1step))
      rm temp_pars.csv
      rm temp_par_traj.csv
      rm temp_traj.csv
    done
    par2=$(($par2+$par2step))
  done
fi # runpdm
cat cohort_1_pdm.csv >>cohort_1_two_param_range.csv

# CIM REVERSE MODEL (EDM - FFLOOP, g_ED = 1)
if [ "$runrev" = true ]
then
  echo "running cohort 1, CIM:REV"
  rm -f cohort_1_rev.csv
  par1step=20.0
  n1runs=3
  par2=1.0
  sed -E -i .bak 's/(par[ ]+MODEL[ ]+)[^ ]+/\14.0/' odexpdir/parameters.pop
  par1=30.0
  for n1 in {1..$n1runs}
  do
    echo "Cohort 1; CIM:REV; $par1name: $par1; $par2name: $par2"
    # set parameters
    sed -E -i .bak 's/(par[ ]+'"$par1name"'[ ]+)[^ ]+/\1'"$par1"'/' odexpdir/parameters.pop
    sed -E -i .bak 's/(par[ ]+'"$par2name"'[ ]+)[^ ]+/\1'"$par2"'/' odexpdir/parameters.pop
    ./model.out >/dev/null # run code

    # extract simulations at TIME = 0 (STEP 0)
    for i in {1..$cohort_size}
    do
      echo "CIM,REV,$par1,$par2," >>temp_pars.csv
    done
    hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 0 { print }' >temp_traj.csv
    lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
    cat temp_par_traj.csv >>!cohort_1_rev.csv
    rm temp_pars.csv

    # extract simulations at TIME = 3 years(STEP 1)
    for i in {1..$cohort_size}
    do
      echo "CIM,REV,$par1,$par2," >>temp_pars.csv
    done
    hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 1 { print }' >temp_traj.csv
    lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
    cat temp_par_traj.csv >>cohort_1_rev.csv
    rm temp_pars.csv

    # extract simulations at TIME = 5475 (STEP 5)
    for i in {1..$cohort_size}
    do
      echo "CIM,REV,$par1,$par2," >>temp_pars.csv
    done
    hexdump -e '"%u," "%d," 53 "%g," "%g\n"' odexpdir/traj.dat | awk -F ',' '$1 == 5 { print }' >temp_traj.csv
    lam temp_pars.csv temp_traj.csv >temp_par_traj.csv
    cat temp_par_traj.csv >>cohort_1_rev.csv
    par1=$(($par1+$par1step))
    rm temp_pars.csv
    rm temp_par_traj.csv
    rm temp_traj.csv
  done
fi # runrev
cat cohort_1_rev.csv >>cohort_1_two_param_range.csv

rm virtual_cohort.csv

mv cohort_1_two_param_range.csv "${path_to_results}cohort_1_two_param_range.csv"


exit 0
