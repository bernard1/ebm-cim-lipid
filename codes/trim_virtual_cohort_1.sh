#!/bin/zsh

set -eu # makes your program exit on error or unbound variable

progname=$0

function usage {
    echo "usage: $(basename $progname) TODO:ENTER USAGE"
    exit 1
}

path_to_results="results/"

# remove subject with BW0 outside range 63.8 147.8
# remove subject with H outside range 1.530373 1.941122
awk -F ',' 'NR == 1 {print}; \
            $33 > 63.8 && $33 < 147.8 && \
            $34 > 1.53 && $34 < 1.94  \
            { print }' "${path_to_results}"cohort_1_two_param_range.csv >"${path_to_results}"cohort_1_two_param_range_trimmed.csv



# ; awk -F ',' 'NR == 1 { for(i=1;i<=NF;i++) print i, $i }' cohort_1_two_param_range.csv
# 1 MODEL
# 2 VARIANT
# 3 carbs
# 4 g_ED
# 5 STEP
# 6 ID
# 7 TIME
# 8 F
# 9 L
# 10 G
# 11 AT
# 12 AGE
# 13 GL
# 14 AGE_F
# 15 BW
# 16 BMI
# 17 p
# 18 RMR
# 19 delta
# 20 DeltaEI
# 21 beta_ED
# 22 aux_P
# 23 aux_X
# 24 aux_D
# 25 EE50
# 26 EE
# 27 ED
# 28 EI
# 29 TEF
# 30 DeltaE_CIM
# 31 carb_flux
# 32 Kin
# 33 BW0
# 34 H
# 35 AGE0
# 36 PAL
# 37 carbs0
# 38 rho_F
# 39 rho_L
# 40 rho_G
# 41 gam_F
# 42 gam_L
# 43 eta_F
# 44 eta_L
# 45 beta_TEF
# 46 beta_AT
# 47 tau_AT
# 48 C
# 49 rmr_c
# 50 Fm
# 51 Fw
# 52 F0
# 53 L0
# 54 RMR0
# 55 EI0
# 56 K
# 57 CI
# 58 kg
# 59 beta_uptake
# 60 Kin0




