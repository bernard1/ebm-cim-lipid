# Supplementary datasets and codes for the manuscript

  **Implication of lipid turnover for the control of energy balance**

S. Bernard and K.L. Spalding

## List of files

- [README.md](README.md) this file
- [runall.sh](runall.sh) shell script to generate the numerical simulations and the figures

### codes

- [batch_cohort_1.sh](codes/batch_cohort_1.sh) run cohort V1
- [batch_cohort_2.sh](codes/batch_cohort_2.sh) run cohort V2
- [dynamical_models.pop](codes/dynamical_models.pop) implementation of the models
- [traj_varnames.txt](codes/traj_varnames.txt) 
- [trial_cohort_1.sh](codes/trial_cohort_1.sh) run virtual trial
- [trim_cohort_1_trial.sh](codes/trim_cohort_1_trial.sh) trim virtual trial 
- [trim_virtual_cohort_1.sh](codes/trim_virtual_cohort_1.sh) trim cohort V1
- [trim_virtual_cohort_2.sh](codes/trim_virtual_cohort_2.sh) trim cohort V2
- [odexp-v1.0.3/](codes/odexp-v1.0.3/) codes to run simulations

### datasets

- [virtual_cohort_1.csv](datasets/virtual_cohort_1.csv) virtual patients, cohort V1
- [virtual_cohort_2.csv](datasets/virtual_cohort_2.csv) virtual patients, cohort V2
- [nominal_stats_cohort_V1.csv](datasets/nominal_stats_cohort_V1.csv) population parameters used to generate cohort V1
- [nominal_stats_cohort_V2.csv](datasets/nominal_stats_cohort_V2.csv) population parameters used to generate cohort V2
- [nominal_trial_cohort_1_stats.csv](datasets/nominal_trial_cohort_1_stats.csv) population parameters used to generate virtual trial cohort
- [summary_cohort_V1.csv](datasets/summary_cohort_V1.csv) summary statistics, cohort V1
- [summary_cohort_V2.csv](datasets/summary_cohort_V2.csv) summary statistics, cohort V2
- [summary_trial_cohort_V1.csv](datasets/summary_trial_cohort_V1.csv) summary statistics, trial cohort  

For reproducibility, the simulation results are also stored here.

- [cohort_1_two_param_range_trimmed.csv](datasets/cohort_1_two_param_range_trimmed.csv) trimmed simulation results for cohort V1
- [cohort_2_two_param_range_trimmed.csv](datasets/cohort_2_two_param_range_trimmed.csv) trimmed simulation results for cohort V2
- [cohort_1_short_CIM_trial_trimmed.csv](datasets/cohort_1_short_CIM_trial_trimmed.csv) trimmed simulation results for the short trial 

# Numerical simulations and figure generation

Dependencies

- A C compiler, such as `gcc`.
- The GNU Scientific Library (GSL) [https://www.gnu.org/software/gsl/]((available here)).
- `cmake` version 3.6 or above.

To generate the numerical simulations and the figures, run the command line 

```
./runall.sh
```

The script will run all the steps described below. 

# Details 

The simulations use the solver `odexp`. The source files are in the folder
[codes/odexp-v1.0.2/](codes/odexp-v1.0.2/). 
The solver depends on the GSL [https://www.gnu.org/software/gsl/]((available here)).


## Build and install `odexp` locally

```
cd codes/odexp-v1.0.3/
rm -fr build
mkdir -p build && cd build
builddir=`pwd`
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$builddir ..
make 
make install
export PATH=$PATH:`pwd`/bin
library_path=`pwd`/lib
PKG_CONFIG_PATH+=`pwd`/share/pkgconfig
export PKG_CONFIG_PATH
export CFLAGS=`pkg-config --cflags-only-I odexp`
LFLAGS=`pkg-config --libs-only-L odexp` 
LFLAGS+=" -Wl,-rpath $library_path"
export LFLAGS
echo $CFLAGS
echo $LFLAGS
cd $basedir
```

## Numerical simulations

### Cohort V1

To perform the numerical simulations for cohort V1, execute the script `codes/batch_cohort_1.sh`, 
This will generate the dataset `results/cohort_1_two_param_range.csv`. To extract the subset of subjects to include in the analysis,
execute the script `./codes/trim_virtual_cohort_1.sh`. This will generate the dataset `results/cohort_1_two_param_range_trimmed.csv`.


```
./codes/batch_cohort_1.sh && ./codes/trim_virtual_cohort_1.sh
```

### Cohort V2

To perform the numerical simulations for cohort V2, execute the script `codes/batch_cohort_2.sh`, 
This will generate the dataset `results/cohort_2_two_param_range.csv`. To extract the subset of subjects to include in the analysis,
execute the script `codes/trim_virtual_cohort_2.sh`. This will generate the dataset `results/cohort_2_two_param_range_trimmed.csv`.

```
./codes/batch_cohort_2.sh && ./codes/trim_virtual_cohort_2.sh
```

### Two-month virtual trial

To perform the numerical simulations for the two-month virtual trial, execute the script `codes/trial_cohort_1.sh`, 
This will generate the dataset `results/cohort_1_short_CIM_trial.csv`. To extract the subset of subjects to include in the analysis,
execute the script `codes/trim_cohort_1_trial.sh`. This will generate the dataset `results/cohort_1_short_CIM_trial_trimmed.csv`.

```
./codes/trial_cohort_1.sh && ./codes/trim_cohort_1_trial.sh
```
### Simulation results

All simulation results are stored in the [results](results) folder:

- `cohort_1_short_CIM_trial.csv` raw simulation results, virtual trial
- `cohort_1_short_CIM_trial_trimmed.csv` trimmed simulation results for figures, virtual trial
- `cohort_1_two_param_range.csv` raw simulation results, cohort V1
- `cohort_1_two_param_range_trimmed.csv` trimmed simulation results for figures, cohort V1
- `cohort_2_two_param_range.csv` raw simulation results, cohort V2
- `cohort_2_two_param_range_trimmed.csv` trimmed simulation results for figures, cohort V2

The simulation results are also stored in the [datasets](datasets) folder: 

- [cohort_1_two_param_range_trimmed.csv](datasets/cohort_1_two_param_range_trimmed.csv) trimmed simulation results for cohort V1
- [cohort_2_two_param_range_trimmed.csv](datasets/cohort_2_two_param_range_trimmed.csv) trimmed simulation results for cohort V2
- [cohort_1_short_CIM_trial_trimmed.csv](datasets/cohort_1_short_CIM_trial_trimmed.csv) trimmed simulation results for the short trial 

## Figures

To generate the published figures from the simulation datasets, execute the `R` script file in the `figures` folder: 
[figures/generate_figures.R](figures/generate_figures.R). 

```
cd figures
R -f generate_figures.R
```




